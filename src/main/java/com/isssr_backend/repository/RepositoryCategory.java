package com.isssr_backend.repository;

import com.isssr_backend.entity.product.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alberto on 05/06/2017.
 */
@Repository
public interface RepositoryCategory extends MongoRepository<Category, String> {

    List<Category> findAll();

    /**
     * Restituisce al front end una lista di categorie che cominciano con il primo carattere digitato
     *
     * @param str stringa inserita dall'utente durante la digitazione (primo carattere)
     * @return List<Category>
     */
    @Query(fields = "{description : 1,name:1, _id : 1}")
    ArrayList<Category> findCategoryByNameContainingAndUtenteOrUtenteAndNameContaining(String str, String utente, String utente2, String contain);


    /**
     * Metodo GET che restituisce una categoria data la sua description (SOSTITUIBILE VOLENDO CON CAMPO ID SE FRONT END CE LO PASSA)
     *
     * @param name campo descrizione selezionato dall'utente
     * @return Category ritorna la categoria appartenente a quell'ID
     */
    Category findCategoryByName(String name);

    Category findCategoriesByUtenteAndNameContaining(String utente, String categoria);

    void removeCategoryByName(String name);


}
