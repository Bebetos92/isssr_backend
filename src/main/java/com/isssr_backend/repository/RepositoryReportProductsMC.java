package com.isssr_backend.repository;

import com.isssr_backend.entity.product.Product;
import com.isssr_backend.entity.product.ReportProductsMC;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 26/06/2017.
 */
@Repository
public interface RepositoryReportProductsMC extends MongoRepository<ReportProductsMC, String> {

    /**
     * Usata per restituire tutti i report di un certo prodotto con data di arrivo compresa tra 2 date considerate per l'analisi
     */
    //REPO USATA PER CALCOLO COSTO E QUANTITA'
    ArrayList<ReportProductsMC> findReportProductsMCByProductAndArriveDateBetween(Product prod, Date start, Date end);

    //REPO USATA PER DEPERIBILITA'
    ArrayList<ReportProductsMC> findReportProductsMCByProductAndExpiredDateBetween(Product product, Date start, Date end);

    //REPO USATA PER ORDINAZIONI
    ArrayList<ReportProductsMC> findReportProductsMCByProductAndOrderDateBetween(Product product, Date start, Date end);

    //REPO USATA PER QUANTITA DISPONIBILE
    ArrayList<ReportProductsMC> findReportProductsMCByProductAndArriveDateBeforeAndExpiredDateAfter(Product product, Date end, Date start);

    //REPO USATA PER GIACENZA MEDIA
    ArrayList<ReportProductsMC> findReportProductsMCByProductAndArriveDateBefore(Product product, Date end);

    ArrayList<ReportProductsMC>findAll();


}
