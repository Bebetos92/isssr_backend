package com.isssr_backend.repository;

import com.isssr_backend.entity.product.Batch;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by andre on 02/06/2017.
 */
@Repository
public interface RepositoryBatch extends MongoRepository<Batch, String> {

    List<Batch> findAll();

    @Query(value = "{ 'product.$id' : ?0 }")
    List<Batch> findBatchByProductIdAndPosId(String product, String pos);
}
