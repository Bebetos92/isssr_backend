package com.isssr_backend.repository;

import com.isssr_backend.entity.food.ReportDailyFoods;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created by GM on 04/07/2017.
 */
@Repository
public interface RepositoryDailyReportFoods extends MongoRepository<ReportDailyFoods, String> {
    ArrayList<ReportDailyFoods> findAll();

    //USATA PER LA VARIABILE FOODPRODOTTO
    ArrayList<ReportDailyFoods> findReportDailyFoodsByLocationAndDateBetween(String location, Date start, Date end);

    ReportDailyFoods findReportDailyFoodsByDateAndLocation(Date date, String location);
}
