package com.isssr_backend.repository;

import com.isssr_backend.entity.food.Foods;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by GM on 02/06/2017.
 */
@Repository
public interface RepositoryFood extends MongoRepository<Foods, String> {

    ArrayList<Foods> findAll();

    ArrayList<Foods> findFoodsByNomeEquals();

    @Query(fields = "{description : 1, _id : 1}")
    ArrayList<Foods> findFoodsByNomeEquals(String str);


    Foods findFoodsByNomeAndLocation(String nome, String location);

    ArrayList<Foods> findFoodsByLocation(String location);
}
