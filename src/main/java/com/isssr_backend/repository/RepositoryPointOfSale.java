package com.isssr_backend.repository;

import com.isssr_backend.entity.PointOfSale;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alberto on 02/06/2017.
 */
@Repository
public interface RepositoryPointOfSale extends MongoRepository<PointOfSale, String> {

    //@Query(fields = "{name:1}")
    ArrayList<PointOfSale> findAll();

    @Query(value = "{}", fields = "{name : 1, index : 1, _id : 0}")
    List<PointOfSale> findNameAndIndexAndExcludeId();
}




