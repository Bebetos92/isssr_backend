package com.isssr_backend.repository;

import com.isssr_backend.entity.food.DailyReportPDV;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 04/07/2017.
 */
@Repository
public interface RepositoryDailyReportPDV extends MongoRepository<DailyReportPDV, String> {
    ArrayList<DailyReportPDV> findDailyReportPDVByDataIs(Date date);
}
