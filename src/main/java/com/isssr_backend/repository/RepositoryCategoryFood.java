package com.isssr_backend.repository;

import com.isssr_backend.entity.food.CategoryFood;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.ArrayList;

/**
 * Created by GM on 04/07/2017.
 */
public interface RepositoryCategoryFood extends MongoRepository<CategoryFood, String> {

    ArrayList<CategoryFood> findAll();


    ArrayList<CategoryFood> findCategoryFoodByNameAndUtenteOrUtenteAndName(String name, String Utente, String std, String Name);

    CategoryFood findCategoryFoodByNameEquals(String nameCategory);

    CategoryFood findCategoryFoodByNameEqualsAndLocation(String name, String location);

    CategoryFood findCategoryFoodByNameEqualsAndLocationAndUtente(String name, String location, String utente);

    @Query(fields = "{description : 1, name: 1, _id : 1,location:1}")
    ArrayList<CategoryFood> findCategoryByNameContainingAndUtenteAndLocationOrUtenteAndLocationAndNameContaining(String str1, String utente, String location, String ut, String loc, String str2);

    void removeCategoryByNameAndUtenteAndLocationOrUtenteAndLocationAndName(String name, String utente, String location, String ut, String loc, String str2);

    CategoryFood findCategoryFoodByNameAndLocation(String name,String location);
}
