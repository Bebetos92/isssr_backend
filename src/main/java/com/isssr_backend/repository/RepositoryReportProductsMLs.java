package com.isssr_backend.repository;


import com.isssr_backend.entity.product.Product;
import com.isssr_backend.entity.product.ReportProductsMLs;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 26/06/2017.
 */
@Repository
public interface RepositoryReportProductsMLs extends MongoRepository<ReportProductsMLs, String> {

    // REPO USATA NEL COSTO E NELLE ORDINAZIONI
    ArrayList<ReportProductsMLs> findReportProductsMLsByArriveDateBetweenAndProductAndPosition(Date start, Date end, Product product, String pos);

    // REPO USATA NELLA QUANTITA DISPONIBILE
    ArrayList<ReportProductsMLs> findReportProductsMLsByArriveDateBetweenAndProductAndPositionAndExpiredDateAfter(Date start, Date end, Product product, String pos, Date exp);

    // REPO USATA NELLA DEPERIBILITA
    ArrayList<ReportProductsMLs> findReportProductsMLsByProductAndExpiredDateBetweenAndPosition(Product product, Date start, Date end, String pos);

    //REPO USATA PER GIACENZA MEDIA
    ArrayList<ReportProductsMLs> findReportProductsMLsByArriveDateBefore(Product product, Date end);

    ArrayList<ReportProductsMLs> findAll();


    ArrayList<ReportProductsMLs> findReportProductsMLsByPosition(String postion);
}
