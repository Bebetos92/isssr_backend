package com.isssr_backend.repository;

import com.isssr_backend.entity.Index;
import com.isssr_backend.entity.TopThree;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 09/07/2017.
 */
@Repository
public interface RepositoryTopThree extends MongoRepository<TopThree, String>{

    ArrayList<TopThree> findAll();


    TopThree findTopThreeByDateAndIndex(Date eq, String index);

    void deleteAll();

}
