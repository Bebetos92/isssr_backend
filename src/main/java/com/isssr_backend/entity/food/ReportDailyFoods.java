package com.isssr_backend.entity.food;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 04/07/2017.
 */
public class ReportDailyFoods {
    @Id
    private String id;
    @Field("date")
    private Date date;
    @Field("location")
    private String location;

    @Field("foodSales")
    private ArrayList<ItemDailyReport> foodSale;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<ItemDailyReport> getFoodSale() {
        return foodSale;
    }

    public void setFoodSale(ArrayList<ItemDailyReport> foodSale) {
        this.foodSale = foodSale;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
