package com.isssr_backend.entity.food;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 04/07/2017.
 */
@Document(collection = "DailyReportPDV")
public class DailyReportPDV {

    @Field("pietanzaAnalyticsDtos")
    ArrayList<pietanzaAnalyticsDtos> pietanzaAnalyticsDtos;
    @Id
    private String id;
    @Field("data")
    private Date data;
    @Field("location")
    private String location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<com.isssr_backend.entity.food.pietanzaAnalyticsDtos> getPietanzaAnalyticsDtos() {
        return pietanzaAnalyticsDtos;
    }

    public void setPietanzaAnalyticsDtos(ArrayList<com.isssr_backend.entity.food.pietanzaAnalyticsDtos> pietanzaAnalyticsDtos) {
        this.pietanzaAnalyticsDtos = pietanzaAnalyticsDtos;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
