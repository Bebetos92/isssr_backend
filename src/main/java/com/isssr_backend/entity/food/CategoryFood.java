package com.isssr_backend.entity.food;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by GM on 04/07/2017.
 */
@Component
@Document(collection = "CategoryFood")
public class CategoryFood {

    @Id
    private String id;
    @Field("name")
    private String name;
    @Field("description")
    private String description;
    @Field("food")
    @DBRef
    private ArrayList<Foods> food;
    @Field("utente")
    private String utente;

    @Field("location")
    private String location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Foods> getFood() {
        return food;
    }

    public void setFood(ArrayList<Foods> food) {
        this.food = food;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
