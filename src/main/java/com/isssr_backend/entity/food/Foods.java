package com.isssr_backend.entity.food;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;

/**
 * Created by GM on 04/07/2017.
 */
@Document(collection = "Foods")
public class Foods {

    @Id
    private String id;

    @Field("nome")
    private String nome;

    @Field("mainCategory")
    private String mainCategory;

    @Field("etichette")
    private ArrayList<String> etichette;

    @Field("ingredienti")
    private ArrayList<Ingrediente> ingredienti;

    @Field("location")
    private String location;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<String> getEtichette() {
        return etichette;
    }

    public void setEtichette(ArrayList<String> etichette) {
        this.etichette = etichette;
    }

    public ArrayList<Ingrediente> getIngredienti() {
        return ingredienti;
    }

    public void setIngredienti(ArrayList<Ingrediente> ingredienti) {
        this.ingredienti = ingredienti;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
