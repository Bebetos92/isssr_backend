package com.isssr_backend.entity.food;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

/**
 * Created by GM on 04/07/2017.
 */
@Component
public class PietanzaMenuDto {

    @Id
    private String id;
    @Field("pietanzaDto")
    private PietanzaDto pietanzaDto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PietanzaDto getPietanzaDto() {
        return pietanzaDto;
    }

    public void setPietanzaDto(PietanzaDto pietanzaDto) {
        this.pietanzaDto = pietanzaDto;
    }
}
