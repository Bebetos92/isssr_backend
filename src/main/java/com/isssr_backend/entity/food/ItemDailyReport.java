package com.isssr_backend.entity.food;


import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.stereotype.Component;

/**
 * Created by andre on 05/07/2017.
 */
@Component
public class ItemDailyReport {
    @DBRef
    private Foods foods;

    private int quantitaProdotta;
    private int quantitaConsumata;
    private double price;

    public Foods getFoods() {
        return foods;
    }

    public void setFoods(Foods foods) {
        this.foods = foods;
    }

    public int getQuantitaProdotta() {
        return quantitaProdotta;
    }

    public void setQuantitaProdotta(int quantitaProdotta) {
        this.quantitaProdotta = quantitaProdotta;
    }

    public int getQuantitaConsumata() {
        return quantitaConsumata;
    }

    public void setQuantitaConsumata(int quantitaConsumata) {
        this.quantitaConsumata = quantitaConsumata;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
