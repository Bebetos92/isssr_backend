package com.isssr_backend.entity.ModelMapper;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

/**
 * Created by GM on 29/06/2017.
 */
@Component
public class Ingredient {

    @Field("ingredient")
    private String ingredient;
    @Field("quantity")
    private double quantity;
}
