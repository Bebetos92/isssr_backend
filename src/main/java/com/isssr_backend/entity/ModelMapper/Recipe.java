package com.isssr_backend.entity.ModelMapper;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by GM on 27/06/2017.
 */
@Component
public class Recipe {

    @Field("id")
    private String id;

    @Field("RecipeName")
    private String name;

    @Field("ingredient")
    private ArrayList<Ingredient> ingredients;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
