package com.isssr_backend.entity.ModelMapper;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by GM on 27/06/2017.
 */
public class ItemReport {

    @Id
    private String id;
    @DBRef
    @Field("product")
    private AbstractProducts item;

    @Field("ProdQuantity")
    private int ProdQuantity;

    //launch + dinner quantity
    @Field("ConsQuantity")
    private int ConsQuantity;

    @Field("launchTime")
    private int ConsQuantityLaunch;


    @Field("dinnerTime")
    private int ConsQuantityDinner;

    @Field("price")
    private double price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AbstractProducts getItem() {
        return item;
    }

    public void setItem(AbstractProducts item) {
        this.item = item;
    }

    public int getProdQuantity() {
        return ProdQuantity;
    }

    public void setProdQuantity(int prodQuantity) {
        ProdQuantity = prodQuantity;
    }

    public int getConsQuantity() {
        return ConsQuantity;
    }

    public void setConsQuantity(int consQuantity) {
        ConsQuantity = consQuantity;
    }

    public int getConsQuantityLaunch() {
        return ConsQuantityLaunch;
    }

    public void setConsQuantityLaunch(int consQuantityLaunch) {
        ConsQuantityLaunch = consQuantityLaunch;
    }

    public int getConsQuantityDinner() {
        return ConsQuantityDinner;
    }

    public void setConsQuantityDinner(int consQuantityDinner) {
        ConsQuantityDinner = consQuantityDinner;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
