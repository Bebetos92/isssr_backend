Recipe
{
	name : "tonnarelli al sugo",
	ingredient:[
		{ingredient:"tonnarelli",
		 quantity : 100 
		 },
		 {
		 ingredient:"pelati",
		 quantity :200
		 },
	]
}

Food
{
	name : "Tonnarelli al sugo",
	mainCategory : "primo",
	tags: ["pasta all'uovo" , "spicy" , "tradizionale"],
	recipe : {
			name : "tonnarelli al sugo",
			ingredient:[
				{ingredient:"tonnarelli",
				 quantity : 100 
				 },
				 {
				 ingredient:"pelati",
				 quantity :200
				 },
			]
		},
	expQuantity: 1	
}

Product PDV
{
	name: "air Vigorsol",
	mainCategory: "dolciumi",
	category: ["snack", "fragola"],
	description  :"buonissime per l'igiene orale",
	expiredDate : "2017-12-12"
}

ITEM REPORT
{
	product: {
			name : "Tonnarelli al sugo",
			mainCategory : "primo",
			tags: ["pasta all'uovo" , "spicy" , "tradizionale"],
			recipe : {
					name : "tonnarelli al sugo",
					ingredient:[
						{ingredient:"tonnarelli",
						 quantity : 100 
						 },
						 {
						 ingredient:"pelati",
						 quantity :200
						 },
					]
				},
			expQuantity: 1	
		},
	ProdQuantity: 20,
	ConsQuantity: 18,
	ConsQuantityLaunch: 15,
	ConsQuantityDinner: 3,
	price: 16.50,	
}
{
	product: {
		name: "air Vigorsol",
		mainCategory: "dolciumi",
		category: ["snack", "fragola"],
		description  :"buonissime per l'igiene orale",
		expiredDate : "2017-12-12"
	},
	prodQuantity:50,
	ConsQuantity:30,
	ConsQuantityLaunch:28,
	ConsQuantityDinner:2,
	price: 2
}

DAILYREPORT
{
	item:[
	{
	product: {
			name : "Tonnarelli al sugo",
			mainCategory : "primo",
			tags: ["pasta all'uovo" , "spicy" , "tradizionale"],
			recipe : {
					name : "tonnarelli al sugo",
					ingredient:[
						{ingredient:"tonnarelli",
						 quantity : 100 
						 },
						 {
						 ingredient:"pelati",
						 quantity :200
						 },
					]
				},
			expQuantity: 1	
		},
	ProdQuantity: 20,
	ConsQuantity: 18,
	ConsQuantityLaunch: 15,
	ConsQuantityDinner: 3,
	price: 16.50,	
},
{
	product: {
			name : "Tonnarelli al sugo",
			mainCategory : "primo",
			tags: ["pasta all'uovo" , "spicy" , "tradizionale"],
			recipe : {
					name : "tonnarelli al sugo",
					ingredient:[
						{ingredient:"tonnarelli",
						 quantity : 100 
						 },
						 {
						 ingredient:"pelati",
						 quantity :200
						 },
					]
				},
			expQuantity: 1	
		},
	ProdQuantity: 20,
	ConsQuantity: 18,
	ConsQuantityLaunch: 15,
	ConsQuantityDinner: 3,
	price: 16.50,	
}
{
	product: {
		name: "air Vigorsol",
		mainCategory: "dolciumi",
		category: ["snack", "fragola"],
		description  :"buonissime per l'igiene orale",
		expiredDate : "2017-12-12"
	},
	prodQuantity:50,
	ConsQuantity:30,
	ConsQuantityLaunch:28,
	ConsQuantityDinner:2,
	price: 2
}],
date: "2017-07-1",
location: "Giorgione al molo"
}