package com.isssr_backend.entity.ModelMapper;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 27/06/2017.
 */
public class DailyReport {

    @Id
    private String Id;

    @Field("item")
    private ArrayList<ItemReport> elements;

    @Field("date")
    private Date date;

    @Field("location")
    private String location;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public ArrayList<ItemReport> getElements() {
        return elements;
    }

    public void setElements(ArrayList<ItemReport> elements) {
        this.elements = elements;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
