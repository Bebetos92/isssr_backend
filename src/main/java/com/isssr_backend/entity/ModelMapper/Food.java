package com.isssr_backend.entity.ModelMapper;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;

/**
 * Created by GM on 02/06/2017.
 */
@Document(collection = "Foods")
public class Food extends AbstractProducts {

    @Id
    private String id;

    @Field("name")
    private
    String name;

    //it means main course, starter, dessert, etc
    @Field("mainCategory")
    private String mainCategory;

    @Field("tags")
    private
    ArrayList<String> tags;

    @DBRef
    @Field("recipe")
    private Recipe recipe;

    //quanto tempo si mantiene la pietanza 1,2,3  giorni etc
    @Field("expQuantity")
    private int expQuantity;


    public Food() {
    }

    public Food(String name, String mainCategory) {
        this.name = name;
        this.mainCategory = mainCategory;
    }


    public void toResume() {
        System.out.println("name:" + name + " main category: " + mainCategory);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public int getExperienceDay() {
        return expQuantity;
    }

    public void setExperienceDay(int expQuantity) {
        this.expQuantity = expQuantity;
    }
}
