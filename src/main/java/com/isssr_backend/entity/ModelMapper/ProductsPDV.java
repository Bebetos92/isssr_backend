package com.isssr_backend.entity.ModelMapper;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 27/06/2017.
 */
@Document(collection = "productsPDV")
public class ProductsPDV extends AbstractProducts {
    @Id
    private String id;
    @Field("name")
    private String name;
    @Field("mainCategory")
    private String mainCategory;
    @Field("category")
    private ArrayList<String> category;
    @Field("description")
    private String description;
    @Field("expiredDate")
    private Date expiredDay;


    public ProductsPDV() {
    }

    public void toResume() {
        System.out.println("name : " + name + " category: " + category.toString() + " Description : " + description
                + " ExpiredDay:" + expiredDay);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<String> category) {
        this.category = category;
    }

    public Date getExpiredDay() {
        return expiredDay;
    }

    public void setExpiredDay(Date expiredDay) {
        this.expiredDay = expiredDay;
    }
}
