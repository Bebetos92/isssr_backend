package com.isssr_backend.entity.message;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import javax.persistence.Id;
import java.util.Date;


@Component
public class MessageProduct {

    @Id
    private ObjectId id;

    @Field("category")
    private ObjectId category;

    @Field("product")
    private ObjectId product;

    @Field("index")
    private ObjectId index;

    @Field("pos")
    private ObjectId pos;

    @Field("dateStart")
    private Date dateStart;

    @Field("dateStop")
    private Date dateStop;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getCategory() {
        return category;
    }

    public void setCategory(ObjectId category) {
        this.category = category;
    }

    public ObjectId getProduct() {
        return product;
    }

    public void setProduct(ObjectId product) {
        this.product = product;
    }

    public ObjectId getIndex() {
        return index;
    }

    public void setIndex(ObjectId index) {
        this.index = index;
    }

    public ObjectId getPos() {
        return pos;
    }

    public void setPos(ObjectId pos) {
        this.pos = pos;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateStop() {
        return dateStop;
    }

    public void setDateStop(Date dateStop) {
        this.dateStop = dateStop;
    }
}
