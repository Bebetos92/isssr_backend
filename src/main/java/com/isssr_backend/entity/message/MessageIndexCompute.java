package com.isssr_backend.entity.message;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 28/06/2017.
 */
@Component
public class MessageIndexCompute {

    @Field("start")
    private Date start;
    @Field("end")
    private Date end;
    @Field("granularity")
    private int granularity;
    @Field("descProduct")
    private ArrayList<String> descProduct;
    @Field("index")
    private String index;
    @Field("location")
    private ArrayList<String> location;
    @Field("utente")
    private String utente;


    // necesaria per differenziare indici singoli e categorie
    @Field("flag")
    private String flag;


    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getGranularity() {
        return granularity;
    }

    public void setGranularity(int granularity) {
        this.granularity = granularity;
    }

    public ArrayList<String> getDescProduct() {
        return descProduct;
    }

    public void setDescProduct(ArrayList<String> descProduct) {
        this.descProduct = descProduct;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public ArrayList<String> getLocation() {
        return location;
    }

    public void setLocation(ArrayList<String> location) {
        this.location = location;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public MessageIndexCompute clone() {
        MessageIndexCompute c = new MessageIndexCompute();
        c.setIndex(this.index);
        c.setLocation(this.location);
        c.setDescProduct(this.descProduct);
        c.setEnd(this.end);
        c.setStart(this.start);
        c.setGranularity(this.granularity);
        c.setUtente(this.utente);
        c.setFlag(this.flag);

        return c;
    }
}
