package com.isssr_backend.entity.message;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import javax.persistence.Id;
import java.util.ArrayList;


@Component
public class MessageNewCategory {

    @Id
    private String id;
    @Field("prodotti")
    private ArrayList<String> prodotti;
    @Field("categorie")
    private ArrayList<String> categorie;
    @Field("food")
    private ArrayList<String> food;
    @Field("categorieFood")
    private ArrayList<String> categorieFood;

    @Field("name")
    private String name;
    @Field("description")
    private String description;
    @Field("utente")
    private String utente;

    public ArrayList<String> getFood() {
        return food;
    }

    public void setFood(ArrayList<String> food) {
        this.food = food;
    }

    public ArrayList<String> getCategorieFood() {
        return categorieFood;
    }

    public void setCategorieFood(ArrayList<String> categorieFood) {
        this.categorieFood = categorieFood;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getProdotti() {
        return prodotti;
    }

    public void setProdotti(ArrayList<String> prodotti) {
        this.prodotti = prodotti;
    }

    public ArrayList<String> getCategorie() {
        return categorie;
    }

    public void setCategorie(ArrayList<String> categorie) {
        this.categorie = categorie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
