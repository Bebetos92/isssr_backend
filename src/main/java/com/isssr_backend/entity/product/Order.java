package com.isssr_backend.entity.product;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class Order {

    @Field("id")
    private String id;
    @Field("num_ordinazioni")
    private int num_ordinazioni;
    @Field("destination")
    private String destination;
    @Field("sellPrice")
    private double sellPrice;

    @Field("orderDate")
    private Date orderDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getNum_ordinazioni() {
        return num_ordinazioni;
    }

    public void setNum_ordinazioni(int num_ordinazioni) {
        this.num_ordinazioni = num_ordinazioni;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(double sellPrice) {
        this.sellPrice = sellPrice;
    }
}

