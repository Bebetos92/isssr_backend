package com.isssr_backend.entity.product;

/**
 * Created by andre on 23/06/2017.
 */

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.List;

@Document(collection = "BatchesRelation")
public class BatchesRelation {


    @Id
    private String id;

    @Field("batch")
    @DBRef
    private Batch batch;


    @Field("outBatches")
    @DBRef
    private List<Batch> outBatches;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Batch getBatch() {
        return batch;
    }

    public void setBatch(Batch Batch) {
        this.batch = Batch;
    }

    public List<Batch> getOutBatches() {
        return outBatches;
    }

    public void setOutBatches(List<Batch> outBatches) {
        this.outBatches = outBatches;
    }

}