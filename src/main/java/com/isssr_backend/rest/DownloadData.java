package com.isssr_backend.rest;

import com.isssr_backend.global.GlobalVariable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by andre on 23/06/2017.
 */
public class DownloadData {


    private String out = new String();

    /**
     * Metodo che fa l'update dei batchrelation con chiamata REST a Magazzino Centrale
     *
     * @return String SUCCESS
     */
    public String dataBatchRelCentrale() {

        try {
            URL url = new URL("http://localhost:8080/batchRel/findAll");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            //System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                out = out + output;

            }

            conn.disconnect();

            return out;

        } catch (MalformedURLException e) {

            e.printStackTrace();
            out = "Error";

        } catch (IOException e) {

            e.printStackTrace();
            out = "Error";

        }
        return out;
    }

    /**
     * Funzione che restituisce la lista dei POS direttamente dal magazzino centrale
     *
     * @return Stringa da parsare con una lista dei POS
     */
    public String getAllPos() {

        try {
            URL url = new URL("http://localhost:8080/pos/findAll");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            //System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                out = out + output;

            }

            conn.disconnect();

            return out;

        } catch (MalformedURLException e) {

            e.printStackTrace();
            out = "Error";

        } catch (IOException e) {

            e.printStackTrace();
            out = "Error";

        }
        return out;
    }

    /**
     * Funzione che restituisce dopo aver fornito il campo della data odierna, il daily report del magazzino locale.
     *
     * @param gc calendario per settare la data del giorno prima da cui prendere il report (INTEGRAZIONE)
     * @return String da parsare dal Json
     */
    public String dataDailyReport(GregorianCalendar gc) {

        try {
            String path = "http://localhost:8080/DailyRepPos/findReportPDV?" + GlobalVariable.date + "=";
            int month = gc.get(Calendar.MONTH) + 1;
            int days = gc.get(Calendar.DAY_OF_MONTH);
            String data = "" + gc.get(Calendar.YEAR) + "-" + month + "-" + days;
            path = path + data;
            // TRASFORMAZIONE DELLA DATE
            System.out.println("date : " + path);
            URL url = new URL(path);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            //System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                out = out + output;

            }

            conn.disconnect();

            return out;

        } catch (MalformedURLException e) {

            e.printStackTrace();
            out = "Error";

        } catch (IOException e) {

            e.printStackTrace();
            out = "Error";

        }
        return out;

    }


}
