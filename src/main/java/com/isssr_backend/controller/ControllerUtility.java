package com.isssr_backend.controller;


import com.isssr_backend.entity.MyDate;
import com.isssr_backend.entity.product.BatchesRelation;
import com.isssr_backend.service.ServiceUtility;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;


@Controller
@RequestMapping(path = "/util")
public class ControllerUtility {

    @Autowired
    private ServiceUtility serviceUtility;


    /**
     * Function che chiama l'update dei dati provenienti dal magazzino centrale nel db
     *
     * @return Stringa di avvenuto successo
     * @throws JSONException
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/updateBatchREL", method = RequestMethod.GET)
    public @ResponseBody
    String updateBatchREL() throws IOException {

        return serviceUtility.updateBatchREL();
    }


    /**
     * Function che chiama l'update dei POS nel db
     *
     * @return Stringa di avvenuto successo
     * @throws JSONException
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/updatePos", method = RequestMethod.GET)
    public @ResponseBody
    String updatePos() throws IOException {

        return serviceUtility.updatePos();
    }


    /**
     * Function che chiama l'update dei dati nel db provenienti dal punto vendita
     *
     * @return Stringa di avvenuto successo
     * @throws JSONException
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/dailyFood", method = RequestMethod.GET)
    public @ResponseBody
    String updateDailyReportFood() throws IOException {

        return serviceUtility.updateDailyReportFoods();
    }


    /**
     * Function che chiama l'update dei dati nel db provenienti dal punto vendita
     *
     * @return Stringa di avvenuto successo
     * @throws JSONException
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/updateAll", method = RequestMethod.GET)
    public @ResponseBody
    String updateAll() throws IOException {
        serviceUtility.updateBatchREL();
        serviceUtility.updatePos();
        serviceUtility.updateDailyReportFoods();

         return "ALL UPDATE COMPLETED";
    }


    /**
     * TESTING Function che genera i prodotti assegnandoli randomicamente alle categorie
     *
     * @return Una stringa di avvenuto update
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/upCat", method = RequestMethod.GET)
    public String updateCategories() {

        return serviceUtility.updateCategorie();
    }


    /**
     * This is a Controller used for creating a BatchRelation for useful data analysis.
     *
     * @param mDate a MyDate with date information.
     * @param prodIndex the ArrayList index for the product (WARNING! no control on it).
     * @return the BatchRelation on successfull save.
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/upBatchRel={prodIndex}", method = RequestMethod.POST)
    public @ResponseBody BatchesRelation updateBatchRelation(@RequestBody MyDate mDate, @PathVariable int prodIndex){
        return serviceUtility.createRandomBatchRelation(mDate, prodIndex);
    }
}
