package com.isssr_backend.controller;

import com.isssr_backend.entity.PointOfSale;
import com.isssr_backend.service.ServicePointOfSale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/pos")
public class ControllerPointOfSale {

    @Autowired
    private ServicePointOfSale servicePointOfSale;

    /**
     * This GET call returns all the PointOfSale in the database
     *
     * @return List<PointOfSale>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    List<PointOfSale> getAll() {
        return servicePointOfSale.getAll();
    }

    /**
     * This POST call adds a new PointOfSale from the RequestBody
     *
     * @return PointOfSale ,the PointofSale just added
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addPos", method = RequestMethod.POST)
    public @ResponseBody
    PointOfSale addNewPOS(@RequestBody PointOfSale pointOfSale) {
        return servicePointOfSale.addNewPOS(pointOfSale);
    }

}
