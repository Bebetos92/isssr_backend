package com.isssr_backend.controller.controllerFood;

import com.isssr_backend.entity.food.DailyReportPDV;
import com.isssr_backend.entity.food.ReportDailyFoods;
import com.isssr_backend.entity.food.DailyStoreRoom;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.repository.RepositoryDailyReportFoods;
import com.isssr_backend.repository.RepositoryDailyStoreRoom;
import com.isssr_backend.service.serviceFood.ServiceDailyReportPDV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 04/07/2017.
 */
@Controller
@RequestMapping(path = "/DailyRepPos")
public class ControllerDailyReportPos {

    @Autowired
    private RepositoryDailyStoreRoom mrepo;
    @Autowired
    private ServiceDailyReportPDV serviceDailyReportPDV;

    @Autowired
    private RepositoryDailyReportFoods mRepoDailyReportFood;

    @RequestMapping(path = "/addDailyRepPos", method = RequestMethod.POST)
    public @ResponseBody
    DailyReportPDV addDaily(@RequestBody DailyReportPDV d) {
        d.getData().setHours(0);
        return serviceDailyReportPDV.addReport(d);
    }

    @RequestMapping(path = "/findReportPDV", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<DailyReportPDV> findReport(@RequestParam(GlobalVariable.date) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws ParseException {

        return serviceDailyReportPDV.findReport(date);
    }

    @RequestMapping(path = "/test", method = RequestMethod.GET)
    public @ResponseBody
    DailyStoreRoom getAllByPosition(@RequestParam(GlobalVariable.date) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date, @RequestParam String location) {
        return mrepo.findDailyStoreRoomByDateIsAndLocation(date, location);
    }

    @RequestMapping(path = "/allReport", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<ReportDailyFoods> findAll() {
        return mRepoDailyReportFood.findAll();
    }

}
