package com.isssr_backend.controller.controllerFood;

import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.repository.RepositoryDailyStoreRoom;
import com.isssr_backend.service.serviceFood.ServiceFoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Created by GM on 02/06/2017.
 */

@Controller
@RequestMapping(path = "/food")
public class ControllerFood {

    @Autowired
    ServiceFoods serviceFoods;

    @Autowired
    RepositoryDailyStoreRoom mrepo;

    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Foods> getAll() {
        return serviceFoods.findAll();
    }

    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addFood", method = RequestMethod.POST)
    public @ResponseBody
    Foods addNewFood(@RequestBody Foods food) {
        return serviceFoods.addFoods(food);
    }

    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findFoodLocation", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Foods> getAllByPosition(@RequestParam String location) {
        return serviceFoods.findAllByLocation(location);
    }


}

