package com.isssr_backend.controller;

import com.isssr_backend.service.ServiceReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@Controller
@RequestMapping(path = "/report")
public class ControllerReport {
    @Autowired
    ServiceReport serviceReport;

    /**
     * This is the Controller for download the pdf file.
     *
     * @param fileName the String with the file name to download through @PathVariable.
     * @param response the HttpServletResponse for the sending the file.
     * @throws IOException
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(value = "/download={utente}/{fileName}", method = RequestMethod.GET)
    public void downloadFile(@PathVariable String utente, @PathVariable String fileName, HttpServletResponse response) throws IOException {
        serviceReport.downloadFile(utente, fileName, response);
    }

    /**
     * This is the Controller for get all the report created.
     *
     * @return an ArrayList String with all the files name through @ResponseBody.
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<String> getAllReports(@RequestParam String utente) {
        return serviceReport.getAllReports(utente);
    }


    /**
     * This is the Controller for delete a report.
     *
     * @param name the String for the file name to delete through @PathVariable.
     * @return a message of success/unsuccess through @ResponseBody.
     * @throws IOException
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(value = "/{utente}/deleteReport={name}", method = RequestMethod.DELETE)
    public @ResponseBody
    String deleteReport(@PathVariable String utente,@PathVariable String name) throws IOException {
        return serviceReport.deleteReport(name,utente);
    }

    /**
     * This is a Controller for send the images of a report.
     *
     * @param folder   the String with the file inside through @PathVariable.
     * @param name     the String with the file name through @PathVariable.
     * @param response the HttpServletResponse for sending the image.
     * @throws IOException
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(value = "/{utente}/img={folder}/{name}", produces = MediaType.IMAGE_PNG_VALUE, method = RequestMethod.GET)
    public void getImg(@PathVariable String folder,@PathVariable String utente, @PathVariable String name, HttpServletResponse response) throws IOException {
        serviceReport.sendImg(folder, name, response,utente);
    }


    /**
     * This is a Controller for getting the lastest created reports.
     *
     * @return an ArrayList with all the file names.
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(value = "/getLast", method = RequestMethod.GET)
    public @ResponseBody ArrayList<String> getLastestReports(@RequestParam String utente){
        return serviceReport.getLast5Report(utente);
    }

}
