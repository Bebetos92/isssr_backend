package com.isssr_backend.controller.controllerProduct;

import com.isssr_backend.entity.message.MessageNewCategory;
import com.isssr_backend.entity.product.Category;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.service.serviceProduct.ServiceCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/cat")
public class ControllerCategory {

    @Autowired
    private ServiceCategory serviceCategory;

    /**
     * Chiamata GET che restituisce tutte le categorie, usata per TEST
     *
     * @return List<Category>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    List<Category> getAll() {
        return serviceCategory.getAll();
    }

    /**
     * Chiamata POST per aggiungere una nuova categoria usata per TEST
     *
     * @param mCategory parametro passato dal frontend nel body
     * @ Category ritorna l'oggetto in JSON a schermo
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addCat", method = RequestMethod.POST)
    public @ResponseBody
    Category addNewCategory(@RequestBody Category mCategory) {
        return serviceCategory.addNewCategory(mCategory);
    }

    /**
     * Chiamata usata per riempire la select categorie. Permette di identificare una categoria che comincia con una stringa str digitata
     *
     * @param str Parametro fornito per poter ricercare la categoria
     * @return
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/riempiCat", method = RequestMethod.GET)
    public @ResponseBody
    List<Category> riempiSelectCategorie(@RequestParam String str, @RequestParam String utente) {

        return serviceCategory.riempiSelectCategorie(str, utente);
    }


    /**
     * Funzione che dati come parametro una categoria e una stringa di lettere restituisce il set di prodotti che contengono le 3 lettere
     * e che fanno parte della categoria
     *
     * @param category  parametro categoria precedentemente selezionato
     * @param threechar parametro delle prime 3 lettere
     * @return ArrayList<Product>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/riempiProduct", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Product> riempiSelectProdotti(@RequestParam String category, @RequestParam String threechar) {

        return serviceCategory.riempiSelectProdotti(category, threechar);
    }


    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addNewCat", method = RequestMethod.POST)
    public @ResponseBody
    Category addNewCat(@RequestBody MessageNewCategory message) {
        Category ret = serviceCategory.addCustomCategory(message);
        return ret;
    }

    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/removeCat", method = RequestMethod.DELETE)
    public @ResponseBody
    void removeCat(@RequestParam String name) {
        serviceCategory.removeCategory(name);
        return;
    }

}
