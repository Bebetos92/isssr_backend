package com.isssr_backend.controller.controllerProduct;

import com.isssr_backend.entity.product.Product;
import com.isssr_backend.service.serviceProduct.ServiceProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/product")
public class ControllerProduct {

    @Autowired
    private ServiceProduct serviceProduct;


    /**
     * Chiamata che restituisce tutti i prodotti  Metodo
     * sintassi GET
     * localhost:8080/product/findAll
     *
     * @return List<Product>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    List<Product> getAll() {
        // This returns a JSON or XML with the users
        return serviceProduct.findAll();
    }

    /**
     * @param product Json impacchettato in questo modo :
     *                {
     *                "index": 0,
     *                "name": "vino",
     *                "price": 2.02,
     *                "category": [
     *                "duis",
     *                "non",
     *                "aliquip"
     *                ],
     *                "brand": "IDETICA",
     *                "expiredTime": "2014-01-23"
     *                }
     * @return Product aggiunto in JSON
     */
    @RequestMapping(path = "/addProduct", method = RequestMethod.POST)
    public @ResponseBody
    Product addNewProd(@RequestBody Product product) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        return serviceProduct.addNewProd(product);
    }

    /**
     * Chiamata per crea nuova categoria che trova un insieme di prodotti contenenti caratteri passati nella get.
     *
     * @param fourchar Prime 3 lettere della ricerca
     * @return List<Product> Che appartengono alla categoria selezionata e che iniziano per le prime 3 lettere
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findProdottiByString", method = RequestMethod.GET)
    public @ResponseBody
    List<Product> riempiSelect(@RequestParam String fourchar) {

        return serviceProduct.riempiSelect(fourchar);

    }


}

