package com.isssr_backend.controller.controllerProduct;

import com.isssr_backend.entity.product.Product;
import com.isssr_backend.entity.product.ReportProductsMC;
import com.isssr_backend.repository.RepositoryProduct;
import com.isssr_backend.repository.RepositoryReportProductsMC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 29/06/2017.
 */
@Controller
@RequestMapping(path = "/reportProdMC")
public class ControllerRelationMC {

    @Autowired
    RepositoryReportProductsMC mRepoMC;


    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<ReportProductsMC> getAll() throws ParseException {

        return mRepoMC.findAll();
    }
}
