package com.isssr_backend.controller.controllerProduct.testVariables;

import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.service.serviceProduct.ServiceProduct;
import com.isssr_backend.variables.Variable;
import com.isssr_backend.variables.VariableFactory;
import com.isssr_backend.variables.util.Result;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 08/07/2017.
 */
@Controller
@RequestMapping("/testVar")
public class ControllerVariables {

    @Autowired
    private AutowireCapableBeanFactory beanFactory;


    /**
     * Esempio di messaggio del Frontend per consentire di testare solo la variabile Costo modificando i parametri insieriti in mIndexProd.
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws ParseException
     */

    @RequestMapping(path = "/testCosto", method = RequestMethod.GET)
    public @ResponseBody
    String testCosto() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("strawberry XXXXXXX");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("30/06/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("28/05/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);

        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.MC);
        mIndexProd.setLocation(location);

        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("Costo");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeProduct(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }


    /**
     * Esempio di messaggio del Frontend per consentire di testare solo la variabile Costo modificando i parametri insieriti in mIndexProd.
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws ParseException
     */
    @RequestMapping(path = "/testQuantita", method = RequestMethod.GET)
    public @ResponseBody
    String testQuantita() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {


        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("strawberry XXXXXXX");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("30/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("28/05/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);

        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.MC);
        mIndexProd.setLocation(location);
        //mIndexProd.setIndex("calcola prezzo");

        // get the variable
        Variable var = VariableFactory.getVariable("QuantitaOrdinata");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeProduct(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }


    @RequestMapping(path = "/testQuantitaDisp", method = RequestMethod.GET)
    public @ResponseBody
    String testQuantitaDisp() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {


        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("strawberry XXXXXXX");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("19/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("28/08/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);



        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.MC);
        mIndexProd.setLocation(location);
        //mIndexProd.setIndex("calcola prezzo");

        // get the variable
        Variable var = VariableFactory.getVariable("QuantitaDisponibile");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeProduct(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testDeperibilita", method = RequestMethod.GET)
    public @ResponseBody
    String testDeperibilità() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {


        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("strawberry XXXXXXX");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("01/06/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("05/07/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);


        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        mIndexProd.setLocation(location);

        //mIndexProd.setIndex("calcola prezzo");

        // get the variable
        Variable var = VariableFactory.getVariable("Deperito");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeProduct(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }


    @RequestMapping(path = "/testOrdinazioni", method = RequestMethod.GET)
    public @ResponseBody
    String testOrdinazioni() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {


        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("strawberry XXXXXXX");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("30/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("28/05/2017");
        mIndexProd.setGranularity(2);
        mIndexProd.setEnd(end);


        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.MC);
        mIndexProd.setLocation(location);
        //mIndexProd.setIndex("calcola prezzo");

        // get the variable
        Variable var = VariableFactory.getVariable("nOrdini");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeProduct(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testGiacenza", method = RequestMethod.GET)
    public @ResponseBody
    String testGiacenza() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {


        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("strawberry XXXXXXX");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("30/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("28/05/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);

        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.MC);
        mIndexProd.setLocation(location);

        //mIndexProd.setIndex("calcola prezzo");

        // get the variable
        Variable var = VariableFactory.getVariable("GiacenzaMedia");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeProduct(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testCostoCategory", method = RequestMethod.GET)
    public @ResponseBody
    String testCostoCategory() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("pesce");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("1/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("28/05/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);

        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.MC);
        mIndexProd.setLocation(location);

        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("Costo");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeCategoryProduct(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testFoodProdotto", method = RequestMethod.GET)
    public @ResponseBody
    String testFoodProdotto() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("tortellini");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("1/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("1/08/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);


        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        mIndexProd.setLocation(location);

        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("FoodProdotto");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeFood(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testFoodPrezzo", method = RequestMethod.GET)
    public @ResponseBody
    String testFoodPrezzo() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("tortellini");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("1/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("1/08/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);

        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        mIndexProd.setLocation(location);

        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("FoodPrezzo");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeFood(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testFoodCosto", method = RequestMethod.GET)
    public @ResponseBody
    String testFoodCosto() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("tortellini");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("1/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("1/08/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);


        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        mIndexProd.setLocation(location);


        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("FoodCosto");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeFood(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testFoodVenduto", method = RequestMethod.GET)
    public @ResponseBody
    String testFoodVenduto() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("tortellini");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("1/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("1/08/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);


        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        mIndexProd.setLocation(location);

        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("FoodVenduto");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeFood(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testFoodCategory", method = RequestMethod.GET)
    public @ResponseBody
    String testFoodCategory() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mIndexProd = new MessageIndexCompute();
        ArrayList<String> prods = new ArrayList<>();
        prods.add("Piatto Bolognese");
        mIndexProd.setDescProduct(prods);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("1/01/2017");
        mIndexProd.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("1/08/2017");
        mIndexProd.setGranularity(1);
        mIndexProd.setEnd(end);

        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        mIndexProd.setLocation(location);

        mIndexProd.setUtente(GlobalVariable.topManager);
        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("FoodVenduto");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeCategoryFood(mIndexProd);

        // store it
        ArrayList<Result> r = var.getValue();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }

    @RequestMapping(path = "/testGlobalCosto", method = RequestMethod.GET)
    public @ResponseBody
    String testGlobalCosto() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {

        //BLOCCO TEST VARIABILE COSTO
        MessageIndexCompute mexEconomic = new MessageIndexCompute();
        ArrayList<String > s = new ArrayList<>();
        s.add(GlobalVariable.MC);
        mexEconomic.setLocation(s);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date start = sdf.parse("1/01/2017");
        mexEconomic.setStart(start);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date end = sdf.parse("1/08/2017");
        mexEconomic.setGranularity(1);
        mexEconomic.setEnd(end);
        mexEconomic.setUtente(GlobalVariable.topManager);
        //mIndexProd.setIndex("calcola prezzo");
        //FINE BLOCCO TEST VARIABILE COSTO

        // get the variable
        Variable var = VariableFactory.getVariable("FoodVenduto");

        // forza l'ingetion delle variabili autowired
        beanFactory.autowireBean(var);

        // calcola la variabile
        var.computeEconomic(mexEconomic);

        // store it
        ArrayList<Result> r = var.getValueEconomics();

        for (int i = 0; i < r.size(); i++) {
            for (int j = 0; j < r.get(i).getVar().size(); j++) {
                System.out.println("var " + r.get(i).getVar().get(j));
            }
        }
        return "VARIABILE CALCOLATA";
    }


}



