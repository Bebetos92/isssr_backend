package com.isssr_backend.variables;

import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;

/**
 * Created by andre on 10/07/2017.
 */
public class EconomicFoodCosto extends FoodCosto{

    public EconomicFoodCosto() {
        this.semantic = "economic";
    }


    @Override
    public void computeEconomic(MessageIndexCompute msg) {
        resultProduct.clear();

        MessageIndexCompute mex = new MessageIndexCompute();
        mex.setUtente(msg.getUtente());
        mex.setStart(msg.getStart());
        mex.setEnd(msg.getEnd());
        mex.setGranularity(msg.getGranularity());
        mex.setIndex(msg.getIndex());
        mex.setFlag("");

        ArrayList<String> descProd = new ArrayList<>();
        ArrayList<Foods> f;

        ArrayList<String> location = new ArrayList<>();
        ArrayList<Result> toSum = new ArrayList<>();

        for(String loc:msg.getLocation()){
            f = serviceFoods.findAllByLocation(loc);
            for (int i = 0; i <f.size() ; i++) {
                descProd.add(f.get(i).getNome());
            }
            mex.setDescProduct(descProd);

            for(String s: msg.getLocation()) {
                location.clear();
                location.add(s);
                mex.setLocation(location);
                computeFood(mex);

                // salva i result
                System.err.println("Location : " + s);
                for (Result r : resultProduct) {
                    toSum.add(r);

                }

                resultProduct.clear();
            }
        }

        ArrayList<Result> res = aggregateProducts2(toSum);

        System.err.println("aggr : "+ res.get(0).getVar());

        this.resultCategories = res;
        }
    }


