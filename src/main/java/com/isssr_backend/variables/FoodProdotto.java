package com.isssr_backend.variables;

import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.food.ReportDailyFoods;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by GM on 05/07/2017.
 */
public class FoodProdotto extends Variable {

    public FoodProdotto() {
        this.semantic = "food";
    }

    @Override
    public void computeFood(MessageIndexCompute msg) {

        long giorno = 86400000;

        resultProduct.clear();

        for (int i = 0; i < msg.getDescProduct().size(); i++) {
            Foods foods = serviceFoods.findByNameAndLocation(msg.getDescProduct().get(i), msg.getLocation().get(0));
           // System.out.println("DB FOOD:" + foods.getNome());

            //Blocco di codice per evitare che la between di spring tagli le 2 date limite
            long temp = msg.getStart().getTime() - giorno;
            Date start = new Date();
            start.setTime(temp);
           // System.out.println("DATA START : " + start.toString());
            temp = msg.getEnd().getTime() + giorno;
            Date end = new Date();
            end.setTime(temp);
            //System.out.println("DATA END : " + end.toString());

            ArrayList<ReportDailyFoods> reportDailyFoods = repoDRF.findReportDailyFoodsByLocationAndDateBetween(msg.getLocation().get(0), start, end);
            System.out.println("size : " + reportDailyFoods.size());
            switch (msg.getGranularity()) {
                case GlobalVariable.mensile:
                    calculateWithGranularityFoodProdotto(GlobalVariable.mensile, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.bimestrale:
                    calculateWithGranularityFoodProdotto(GlobalVariable.bimestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.trimestrale:
                    calculateWithGranularityFoodProdotto(GlobalVariable.trimestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.semestrale:
                    calculateWithGranularityFoodProdotto(GlobalVariable.semestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.annuale:
                    calculateWithGranularityFoodProdotto(GlobalVariable.annuale, msg, reportDailyFoods, foods);
                    break;
            }
        }

    }

    @Override
    public void computeEconomic(MessageIndexCompute msg) {

    }

    private void calculateWithGranularityFoodProdotto(int granularity, MessageIndexCompute msg, ArrayList<ReportDailyFoods> reportDailyFoods, Foods foods) {
        double foodProdotto = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Blocco istanziazione calendari
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            foodProdotto = 0;
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            System.out.println();
            for (ReportDailyFoods r : reportDailyFoods) {
                if (calStart.getTimeInMillis() <= r.getDate().getTime() && r.getDate().getTime() < test.getTimeInMillis()) {
                    for (int i = 0; i < r.getFoodSale().size(); i++) {
                        String s = r.getFoodSale().get(i).getFoods().getNome();
                        if (s.equals(foods.getNome())) {
                            foodProdotto += r.getFoodSale().get(i).getQuantitaProdotta();
                        }
                    }
                }
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(foodProdotto);
        }
        resultProduct.add(new Result(resProd));
    }


    @Override
    public void computeProduct(MessageIndexCompute msg) {

        System.out.println("ERRORE SEMANTICA ERRATA");
    }
}

