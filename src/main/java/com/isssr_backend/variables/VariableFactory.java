package com.isssr_backend.variables;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by andre on 23/06/2017.
 */
public class VariableFactory {

    static private String path = "com.isssr_backend.variables.";


    static public Variable getVariable(String type) {
        try {
            return makeNew(type);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return null;
    }


    static private Variable makeNew(String type) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        // trova la classe per nome
        Class c = Class.forName(path + type);

        // instanzazione della classe
        Variable returnMe = (Variable) c.getConstructor().newInstance();

        return returnMe;
    }

    static public String getPath() {
        return path;
    }
}
