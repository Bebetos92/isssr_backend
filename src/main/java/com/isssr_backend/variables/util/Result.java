package com.isssr_backend.variables.util;

import java.util.ArrayList;

/**
 * Created by andre on 29/06/2017.
 */
public class Result {

    private ArrayList<Double> var;

    public Result(ArrayList<Double> var) {
        this.var = var;
    }

    public Result() {
    }

    public ArrayList<Double> getVar() {
        return var;
    }

    public void setVar(ArrayList<Double> var) {
        this.var = var;
    }
}
