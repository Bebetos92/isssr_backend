package com.isssr_backend.variables.util;

import com.isssr_backend.variables.Variable;
import com.isssr_backend.variables.VariableFactory;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Set;

/**
 * Created by emanuele on 28/06/17.
 */
public class ClassFinder {

    private static String path = VariableFactory.getPath();

    ;
    private static boolean initDone = false;
    private static String knownVariables[] = {"product", "food", "economic"};
    private static ArrayList<Mapper> variablesSet = null;

    // chiamata iniziale per popolare le liste di classi variable
    public static void init() {
        if (ClassFinder.initDone == false) {

            // generate array of classes
            variablesSet = new ArrayList<>();

            // init delle liste
            for (String s : knownVariables) {
                variablesSet.add(new Mapper(s, ClassFinder.listMatchingVariables(s)));
            }

            ClassFinder.initDone = true;
        } else {
            System.err.println("WARNING: ClassFinder.init: multiple calls detected.");
        }
    }

    // chiamata per ottenere la lista delle variabili dalla semantica
    static public ArrayList<String> getVariables(String semantic) {
        if (ClassFinder.initDone == true) {

            for (Mapper m : variablesSet) {
                if (m.getMap().equals(semantic)) {
                    return m.getContent();
                }
            }

            // wrong semantic
            System.err.println("ClassFinder.getVariables error: unknown semantic '" + semantic + "'");
            return null;
        }

        // wrong semantic
        System.err.println("ClassFinder.getVariables error: ClassFinder.init() was not called.");
        return null;
    }

    // entry point of the search call
    static private ArrayList<String> listMatchingVariables(String semantic) {

        try {
            return search(semantic);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return null;
    }

    // search function
    static private ArrayList<String> search(String semantic) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        // lista dei nomi utili
        ArrayList<String> returnMe = new ArrayList<>();

        // ottieni tutte le classi implementanti "Variable"
        Reflections reflections = new Reflections(path);
        Set<Class<? extends Variable>> classes = reflections.getSubTypesOf(Variable.class);

        // istanzia ognuna di esse e testa la semantica
        for (Class c : classes) {

            // la semantica è applicabile
            if (((Variable) c.getConstructor().newInstance()).testSemantic(semantic)) {
                returnMe.add(new String(c.getSimpleName()));
            }
        }

        return returnMe;
    }

    public static String[] getKnownVariables() {
        return knownVariables;
    }

    // classe privata usare per matchare liste con chiavi stringhe
    private static class Mapper {

        private String map = null;
        private ArrayList<String> content = null;

        public Mapper(String map, ArrayList<String> content) {
            this.map = map;
            this.content = content;
        }

        public String getMap() {
            return map;
        }

        public ArrayList<String> getContent() {
            return content;
        }
    }
}
