package com.isssr_backend.variables;

import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.entity.product.ReportProductsMC;
import com.isssr_backend.entity.product.ReportProductsMLs;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by andre on 02/07/2017.
 */
public class QuantitaOrdinata extends Variable {


    // Il costruttore setta solo la semantica della variabile.
    public QuantitaOrdinata() {
        this.semantic = "product";
    }


    @Override
    public void computeFood(MessageIndexCompute msg) {

    }

    @Override
    public void computeEconomic(MessageIndexCompute msg) {

    }

    @Override
    public void computeProduct(MessageIndexCompute msg) {

        //Quantità di 1 Giorno in millisecondi usata per il between
        long giorno = 86400000;
        resultProduct.clear();

        for (int i = 0; i < msg.getDescProduct().size(); i++) {
            Product prod = servPr.findByDesc(msg.getDescProduct().get(i));
            //System.out.println("DB PRODOTTO:"+prod.getDescription());
            System.out.println("MSG LOCATION:" + msg.getLocation());
            //Blocco di codice per evitare che la between di spring tagli le 2 date limite
            long temp = msg.getStart().getTime() - giorno;
            Date start = new Date();
            start.setTime(temp);
            System.out.println("DATA START : " + start.toString());
            temp = msg.getEnd().getTime() + giorno;
            Date end = new Date();
            end.setTime(temp);
            System.out.println("DATA END : " + end.toString());

            //Parte relativa all'analisi nel magazzino centrale
            if ((msg.getLocation().get(0)).equals(GlobalVariable.MC)) {
                //Chiamata a tutti i Batch presenti nel MC compresi tra i periodi selezionati e il tipo di prodotto selezionato nel MSG
                ArrayList<ReportProductsMC> ordersReports = repoMC.findReportProductsMCByProductAndArriveDateBetween(prod, start, end);

                switch (msg.getGranularity()) {

                    case GlobalVariable.mensile:
                        calculateWithGranularityQuantityMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateWithGranularityQuantityMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateWithGranularityQuantityMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateWithGranularityQuantityMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateWithGranularityQuantityMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                }
            }
            //Caso dei magazzini Locali
            else {
                ArrayList<ReportProductsMLs> ordersReports = repoML.findReportProductsMLsByArriveDateBetweenAndProductAndPosition(start, end, prod, msg.getLocation().get(0));
                switch (msg.getGranularity()) {

                    case GlobalVariable.mensile:
                        calculateWithGranularityQuantityML(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateWithGranularityQuantityML(GlobalVariable.bimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateWithGranularityQuantityML(GlobalVariable.trimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateWithGranularityQuantityML(GlobalVariable.semestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateWithGranularityQuantityML(GlobalVariable.annuale, msg, ordersReports, prod);
                        break;
                }
            }
        }


    }

    private void calculateWithGranularityQuantityMC(int granularity, MessageIndexCompute msg, ArrayList<ReportProductsMC> ordersReports, Product prod) {
        //System.out.println("CASE1");
        double quantity = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Blocco istanziazione calendari
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            quantity = 0;
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            for (ReportProductsMC r : ordersReports) {

                if (calStart.getTimeInMillis() <= r.getArriveDate().getTime() && r.getArriveDate().getTime() < test.getTimeInMillis()) {
                    //CALCOLA VARIABILE
                    quantity += r.getQuantity();
                }
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(quantity);
        }
        resultProduct.add(new Result(resProd));
    }

    private void calculateWithGranularityQuantityML(int granularity, MessageIndexCompute msg, ArrayList<ReportProductsMLs> ordersReports, Product prod) {
        //System.out.println("CASE1");
        double quantity = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Blocco istanziazione calendari
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            quantity = 0;
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            for (ReportProductsMLs r : ordersReports) {

                if (calStart.getTimeInMillis() <= r.getArriveDate().getTime() && r.getArriveDate().getTime() < test.getTimeInMillis()) {
                    //CALCOLA VARIABILE
                    quantity += r.getQuantity();
                }
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(quantity);
        }
        resultProduct.add(new Result(resProd));
    }
}
