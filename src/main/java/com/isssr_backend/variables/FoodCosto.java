package com.isssr_backend.variables;

/**
 * Created by andre on 06/07/2017.
 */

import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.food.ReportDailyFoods;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.food.DailyStoreRoom;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Il prezzo viene inteso come unità cibo
 * output il prezzo totale della pietanza nella granularità.
 */
public class FoodCosto extends Variable {


    // Il costruttore setta solo la semantica della variabile.
    public FoodCosto() {
        this.semantic = "food";
    }

    @Override
    public void computeFood(MessageIndexCompute msg) {

        long giorno = 86400000;

        resultProduct.clear();

        for (int i = 0; i < msg.getDescProduct().size(); i++) {
            Foods foods = serviceFoods.findByNameAndLocation(msg.getDescProduct().get(i), msg.getLocation().get(0));

            //Blocco di codice per evitare che la between di spring tagli le 2 date limite
            long temp = msg.getStart().getTime() - giorno;
            Date start = new Date();
            start.setTime(temp);
            temp = msg.getEnd().getTime() + giorno;
            Date end = new Date();
            end.setTime(temp);


            ArrayList<ReportDailyFoods> reportDailyFoods = repoDRF.findReportDailyFoodsByLocationAndDateBetween(msg.getLocation().get(0), start, end);
            switch (msg.getGranularity()) {
                case GlobalVariable.mensile:
                    calculateWithGranularityFoodCosto(GlobalVariable.mensile, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.bimestrale:
                    calculateWithGranularityFoodCosto(GlobalVariable.bimestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.trimestrale:
                    calculateWithGranularityFoodCosto(GlobalVariable.trimestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.semestrale:
                    calculateWithGranularityFoodCosto(GlobalVariable.semestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.annuale:
                    calculateWithGranularityFoodCosto(GlobalVariable.annuale, msg, reportDailyFoods, foods);
                    break;
            }
        }

    }

    @Override
    public void computeEconomic(MessageIndexCompute msg) {

    }

    private void calculateWithGranularityFoodCosto(int granularity, MessageIndexCompute msg, ArrayList<ReportDailyFoods> reportDailyFoods, Foods foods) {

        double foodCosto = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Blocco istanziazione calendari
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            foodCosto = 0;
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            for (ReportDailyFoods r : reportDailyFoods) {
                if (calStart.getTimeInMillis() <= r.getDate().getTime() && r.getDate().getTime() < test.getTimeInMillis()) {
                    for (int i = 0; i < r.getFoodSale().size(); i++) {
                        String s = r.getFoodSale().get(i).getFoods().getNome();
                        if (s.equals(foods.getNome())) {
                            //Query al Daily report store room
                            DailyStoreRoom storeRoom = repositoryDailyStoreRoom.findDailyStoreRoomByDateIsAndLocation(r.getDate(), r.getLocation());
                            if(storeRoom!=null) {
                                if(r.getFoodSale().get(i).getFoods().getIngredienti()!=null) {
                                    for (int k = 0; k < r.getFoodSale().get(i).getFoods().getIngredienti().size(); k++) {
                                        if (storeRoom.getItemStores() != null) {
                                            for (int j = 0; j < storeRoom.getItemStores().size(); j++) {
                                                if (storeRoom.getItemStores().get(j).getProduct().getDescription().equals(r.getFoodSale().get(i).getFoods().getIngredienti().get(k).getNomeProdotto())) {
                                                    //System.out.println("UGUALE INGREDIENTE NEL STORE ROOM");
                                                    //System.out.println(r.getDate());
                                                    foodCosto += storeRoom.getItemStores().get(j).getAvgCosto() * r.getFoodSale().get(i).getFoods().getIngredienti().get(k).getQuantita() * r.getFoodSale().get(i).getQuantitaProdotta();
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(foodCosto);
        }
        resultProduct.add(new Result(resProd));
    }

    @Override

    public void computeProduct(MessageIndexCompute msg) {
        System.out.println("ERRORE SEMANTICA ERRATA");
    }
}


