package com.isssr_backend.variables;

import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.food.ReportDailyFoods;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 10/07/2017.
 */
public class EconomicCosto extends Costo {

    public EconomicCosto() {
        this.semantic = "economic";
    }




    @Override
    public void computeEconomic(MessageIndexCompute msg) {

        resultProduct.clear();
        MessageIndexCompute mex = new MessageIndexCompute();
        mex.setUtente(msg.getUtente());
        mex.setStart(msg.getStart());
        mex.setEnd(msg.getEnd());
        mex.setGranularity(msg.getGranularity());
        mex.setIndex(msg.getIndex());
        mex.setFlag("");

        ArrayList<String> descProd = new ArrayList<>();
        ArrayList<Product> r = new ArrayList<>();

        ArrayList<String> location = new ArrayList<>();
        ArrayList<Result> toSum = new ArrayList<>();

        for(String loc: msg.getLocation()){

            r = repoPro.findAll();
            for (int i = 0; i <r.size() ; i++) {
                descProd.add(r.get(i).getDescription());
            }


            mex.setDescProduct(descProd);


            for(String s: msg.getLocation()) {
                location.clear();
                location.add(s);
                mex.setLocation(location);
                computeProduct(mex);

                for(Result rs : resultProduct) {
                    toSum.add(rs);
                }
                resultProduct.clear();

            }

            ArrayList<Result> res = aggregateProducts2(toSum);
            this.resultCategories = res;
        }
    }




}
