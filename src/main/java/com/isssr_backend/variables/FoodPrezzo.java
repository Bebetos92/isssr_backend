package com.isssr_backend.variables;

import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.food.ReportDailyFoods;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Il prezzo viene inteso come unità cibo
 * output il prezzo totale della pietanza nella granularità.
 */
public class FoodPrezzo extends Variable {


    // Il costruttore setta solo la semantica della variabile.
    public FoodPrezzo() {
        this.semantic = "food";
    }

    @Override
    public void computeFood(MessageIndexCompute msg) {

        long giorno = 86400000;

        resultProduct.clear();

        for (int i = 0; i < msg.getDescProduct().size(); i++) {
            Foods foods = serviceFoods.findByNameAndLocation(msg.getDescProduct().get(i), msg.getLocation().get(0));

            //Blocco di codice per evitare che la between di spring tagli le 2 date limite
            long temp = msg.getStart().getTime() - giorno;
            Date start = new Date();
            start.setTime(temp);
            temp = msg.getEnd().getTime() + giorno;
            Date end = new Date();
            end.setTime(temp);


            ArrayList<ReportDailyFoods> reportDailyFoods = repoDRF.findReportDailyFoodsByLocationAndDateBetween(msg.getLocation().get(0), start, end);
            switch (msg.getGranularity()) {
                case GlobalVariable.mensile:
                    calculateWithGranularityFoodPrezzo(GlobalVariable.mensile, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.bimestrale:
                    calculateWithGranularityFoodPrezzo(GlobalVariable.bimestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.trimestrale:
                    calculateWithGranularityFoodPrezzo(GlobalVariable.trimestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.semestrale:
                    calculateWithGranularityFoodPrezzo(GlobalVariable.semestrale, msg, reportDailyFoods, foods);
                    break;
                case GlobalVariable.annuale:
                    calculateWithGranularityFoodPrezzo(GlobalVariable.annuale, msg, reportDailyFoods, foods);
                    break;
            }
        }

    }

    @Override
    public void computeEconomic(MessageIndexCompute msg) {

    }

    private void calculateWithGranularityFoodPrezzo(int granularity, MessageIndexCompute msg, ArrayList<ReportDailyFoods> reportDailyFoods, Foods foods) {
        double foodPrezzo = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Blocco istanziazione calendari
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            foodPrezzo = 0;
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            for (ReportDailyFoods r : reportDailyFoods) {
                if (calStart.getTimeInMillis() <= r.getDate().getTime() && r.getDate().getTime() < test.getTimeInMillis()) {
                    for (int i = 0; i < r.getFoodSale().size(); i++) {
                        String s = r.getFoodSale().get(i).getFoods().getNome();
                        if (s.equals(foods.getNome())) {
                            foodPrezzo += r.getFoodSale().get(i).getPrice() * r.getFoodSale().get(i).getQuantitaConsumata();
                        }
                    }
                }
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(foodPrezzo);
        }
        resultProduct.add(new Result(resProd));
    }

    @Override

    public void computeProduct(MessageIndexCompute msg) {}
}

