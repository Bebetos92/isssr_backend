package com.isssr_backend.variables;

import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.food.DailyStoreRoom;
import com.isssr_backend.entity.product.Order;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.entity.product.ReportProductsMC;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * FUNZIONA SOLO PER MC PER ORA
 */
public class QuantitaDisponibile extends Variable {


    // Il costruttore setta solo la semantica della variabile.
    public QuantitaDisponibile() {
        this.semantic = "product";
    }

    @Override
    public void computeFood(MessageIndexCompute msg) {

    }

    @Override
    public void computeEconomic(MessageIndexCompute msg) {

    }

    @Override
    public void computeProduct(MessageIndexCompute msg) {

        //Quantità di 1 Giorno in millisecondi usata per il between
        long giorno = 86400000;
        resultProduct.clear();

        //for che trova di volta in volta il product equivalente alla descrizione mandata nel message
        for (int i = 0; i < msg.getDescProduct().size(); i++) {
            Product prod = servPr.findByDesc(msg.getDescProduct().get(i));
            //System.out.println("DB PRODOTTO:"+prod.getDescription());

            System.out.println("MSG LOCATION:" + msg.getLocation());

            /**
             * Blocco di codice per evitare che la between di spring tagli le 2 date limite
             */
            long temp = msg.getStart().getTime() - giorno;
            Date start = new Date();
            start.setTime(temp);
            System.out.println("DATA START : " + start.toString());
            temp = msg.getEnd().getTime() + giorno;
            Date end = new Date();
            end.setTime(temp);
            System.out.println("DATA END : " + end.toString());

            /**
             * PARTE RELATIVA AL MAGAZZINO CENTRALE
             */
            if ((msg.getLocation().get(0)).equals(GlobalVariable.MC)) {
                System.out.println("ZONA MAGAZZINO CENTRALE");
                //Trovo di volta in volta i batch corrispondenti
                //System.out.println("DATI QUERY BATCH:"+prod.getId()+" "+msg.getStart()+" "+msg.getEnd());

                //NON CONSIDERA PACCHI SCADUTI
                ArrayList<ReportProductsMC> ordersReports = repoMC.findReportProductsMCByProductAndArriveDateBeforeAndExpiredDateAfter(prod, end, start);
                //System.out.println("DATA Report: "+ ordersReports.get(0).getArriveDate().toString());

                switch (msg.getGranularity()) {

                    case GlobalVariable.mensile:
                        calculateWithGranularityMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateWithGranularityMC(GlobalVariable.bimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateWithGranularityMC(GlobalVariable.trimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateWithGranularityMC(GlobalVariable.semestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateWithGranularityMC(GlobalVariable.annuale, msg, ordersReports, prod);
                        break;
                }
            }
            /**
             * PARTE RELATIVA AI MAGAZZINI LOCALI
             */
            else {
                ArrayList<DailyStoreRoom> dailyStoreRooms = repositoryDailyStoreRoom.findDailyStoreRoomByDateBetweenAndLocation(start, end, msg.getLocation().get(0));
                switch (msg.getGranularity()) {

                    case GlobalVariable.mensile:
                        calculateWithGranularityML(GlobalVariable.mensile, msg, dailyStoreRooms, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateWithGranularityML(GlobalVariable.bimestrale, msg, dailyStoreRooms, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateWithGranularityML(GlobalVariable.trimestrale, msg, dailyStoreRooms, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateWithGranularityML(GlobalVariable.semestrale, msg, dailyStoreRooms, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateWithGranularityML(GlobalVariable.annuale, msg, dailyStoreRooms, prod);
                        break;
                }
            }
        }
    }

    private void calculateWithGranularityML(int granularity, MessageIndexCompute msg, ArrayList<DailyStoreRoom> dailyStoreRooms, Product prod) {

        //System.out.println("CASE1");
        double quantityDispML = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Calendari istanziati
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());
        ArrayList<Integer> mediaQuantitaLocal = new ArrayList<>();
        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            mediaQuantitaLocal.clear();

            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            quantityDispML = 0;
            for (DailyStoreRoom r : dailyStoreRooms) {
                if (r.getDate().getTime() >= calStart.getTimeInMillis() && r.getDate().getTime() <= test.getTimeInMillis()) {
                    for (int i = 0; i < r.getItemStores().size(); i++) {
                        if (r.getItemStores().get(i).getProduct().getDescription().equals(prod.getDescription())) {
                            mediaQuantitaLocal.add(r.getItemStores().get(i).getStock());
                        }
                    }
                }
            }
            for (int k = 0; k < mediaQuantitaLocal.size(); k++) {
                quantityDispML += mediaQuantitaLocal.get(k);
            }
            if (quantityDispML != 0) {
                quantityDispML = quantityDispML / mediaQuantitaLocal.size();
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(quantityDispML);
        }
        resultProduct.add(new Result(resProd));


    }


    private void calculateWithGranularityMC(int granularity, MessageIndexCompute msg, ArrayList<ReportProductsMC> ordersReports, Product prod) {
        //System.out.println("CASE1");
        double quantityDisp = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Calendari istanziati
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());
        ArrayList<Integer> mediaQ = new ArrayList<>();
        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            mediaQ.clear();
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            quantityDisp = 0;
            for (ReportProductsMC r : ordersReports) {
                if (r.getExpiredDate().getTime() >= calStart.getTimeInMillis()) {
                    int ordini = 0;
                    int tmp = 0;
                    System.out.println("size order + " + r.getOrders().size());
                    for (Order ord : r.getOrders()) {
                        //CONTROLLA CHE IL REPORT SIA COMPRESO TRA LA DATA CONSIDERATA SECONDO LA GRANULARITA'
                        if (ord.getOrderDate().getTime() <= test.getTimeInMillis()) {
                            //CONTROLLA CHE IL REPORT ABBIA SCADENZA COMPATIBILE CON IL PERIODO SELEZIONATO
                            ordini = ordini + ord.getNum_ordinazioni();
                        }
                        tmp = r.getQuantity() - ordini;
                        mediaQ.add(tmp);
                    }

                }
            }
            for (int i = 0; i < mediaQ.size(); i++) {
                quantityDisp += mediaQ.get(i);
            }
            System.out.println("media = " + mediaQ.size());
            if (quantityDisp != 0) {
                quantityDisp = quantityDisp / mediaQ.size();
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(quantityDisp);
        }
        resultProduct.add(new Result(resProd));
    }
}

