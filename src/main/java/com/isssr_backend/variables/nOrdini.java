package com.isssr_backend.variables;

import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.entity.product.ReportProductsMC;
import com.isssr_backend.entity.product.ReportProductsMLs;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by andre on 03/07/2017.
 */
public class nOrdini extends Variable {


    // Il costruttore setta solo la semantica della variabile.
    public nOrdini() {
        this.semantic = "product";
    }


    @Override
    public void computeFood(MessageIndexCompute msg) {

    }

    @Override
    public void computeEconomic(MessageIndexCompute msg) {

    }

    @Override
    public void computeProduct(MessageIndexCompute msg) {

        //Quantità di 1 Giorno in millisecondi usata per il between
        long giorno = 86400000;
        resultProduct.clear();

        //for che trova di volta in volta il product equivalente alla descrizione mandata nel message
        for (int i = 0; i < msg.getDescProduct().size(); i++) {
            Product prod = servPr.findByDesc(msg.getDescProduct().get(i));
            //System.out.println("DB PRODOTTO:"+prod.getDescription());

            System.out.println("MSG LOCATION:" + msg.getLocation());

            /**
             * Blocco di codice per evitare che la between di spring tagli le 2 date limite
             */
            long temp = msg.getStart().getTime() - giorno;
            Date start = new Date();
            start.setTime(temp);
            System.out.println("DATA START : " + start.toString());
            temp = msg.getEnd().getTime() + giorno;
            Date end = new Date();
            end.setTime(temp);
            System.out.println("DATA END : " + end.toString());

            /**
             * PARTE RELATIVA AL MAGAZZINO CENTRALE
             */
            if ((msg.getLocation().get(0)).equals(GlobalVariable.MC)) {
                //System.out.println("ZONA MAGAZZINO CENTRALE");
                //Trovo di volta in volta i batch corrispondenti
                //System.out.println("DATI QUERY BATCH:"+prod.getId()+" "+msg.getStart()+" "+msg.getEnd());

                ArrayList<ReportProductsMC> ordersReports = repoMC.findReportProductsMCByProductAndOrderDateBetween(prod, start, end);
                //System.out.println("DATA Report: "+ ordersReports.get(0).getArriveDate().toString());

                switch (msg.getGranularity()) {

                    case GlobalVariable.mensile:
                        calculateOrdinazioniMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateOrdinazioniMC(GlobalVariable.bimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateOrdinazioniMC(GlobalVariable.trimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateOrdinazioniMC(GlobalVariable.semestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateOrdinazioniMC(GlobalVariable.annuale, msg, ordersReports, prod);
                        break;
                }
            }
            /**
             * PARTE RELATIVA AI MAGAZZINI LOCALI
             */
            else {
                ArrayList<ReportProductsMLs> ordersReports = repoML.findReportProductsMLsByArriveDateBetweenAndProductAndPosition(start, end, prod, msg.getLocation().get(0));
                switch (msg.getGranularity()) {

                    case GlobalVariable.mensile:
                        calculateOrdinazioniML(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateOrdinazioniML(GlobalVariable.bimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateOrdinazioniML(GlobalVariable.trimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateOrdinazioniML(GlobalVariable.semestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateOrdinazioniML(GlobalVariable.annuale, msg, ordersReports, prod);
                        break;
                }
            }
        }
    }


    private void calculateOrdinazioniMC(int granularity, MessageIndexCompute msg, ArrayList<ReportProductsMC> ordersReports, Product prod) {
        //System.out.println("CASE1");
        double nOrdini = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Calendari istanziati
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            nOrdini = 0;
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() >= calEnd.getTimeInMillis()) {
                break;
            }
            for (ReportProductsMC r : ordersReports) {

                if (calStart.getTimeInMillis() <= r.getOrderDate().getTime() && r.getOrderDate().getTime() < test.getTimeInMillis()) {
                    //CALCOLA VARIABILE
                    nOrdini++;
                }
            }

            calStart.add(Calendar.MONTH, granularity);
            resProd.add(nOrdini);
        }
        resultProduct.add(new Result(resProd));
    }


    private void calculateOrdinazioniML(int granularity, MessageIndexCompute msg, ArrayList<ReportProductsMLs> ordersReports, Product prod) {
        //System.out.println("CASE1");
        double nOrdini = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Calendari istanziati
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {
            nOrdini = 0;
            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            for (ReportProductsMLs r : ordersReports) {


                if (calStart.getTimeInMillis() <= r.getArriveDate().getTime() && r.getArriveDate().getTime() < test.getTimeInMillis()) {
                    //CALCOLA VARIABILE
                    nOrdini++;
                }
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            resProd.add(nOrdini);
        }
        resultProduct.add(new Result(resProd));

    }


}
