package com.isssr_backend.jasper;

import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.builder.export.Exporters;
import net.sf.dynamicreports.report.builder.chart.*;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.*;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.definition.ReportParameters;
import net.sf.dynamicreports.report.definition.chart.DRIChartCustomizer;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;

import java.awt.*;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;


public class Report {

    private JasperReportBuilder mReport;
    private String fileName;
    private ArrayList<TextColumnBuilder> mTextColumnBuilder;
    private JRDataSource mDataSource;
    private ArrayList<ComponentBuilder> myCharts;
    private int numberProd;

    /**
     * Constructor for create a report with custom title, header and footer.
     *
     * @param title  a String for the title of the report.
     * @param header a String for the header of the report.
     * @param footer a String for the footer of the report.
     */
    public Report(String title, String header, String footer) {

        StyleBuilder boldStyle = stl.style().bold();
        StyleBuilder columnTitleStyle = stl.style().bold()
                .setBorder(stl.pen1Point())
                .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setBackgroundColor(Color.LIGHT_GRAY);

        StyleBuilder valueStyle = stl.style().setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        this.mReport = report();
        this.mReport.title(cmp.text(title).setStyle(boldStyle))
                .pageHeader(cmp.text(header))
                .pageFooter(cmp.text(footer))
                .setColumnTitleStyle(columnTitleStyle)
                .setColumnStyle(valueStyle)
                .highlightDetailEvenRows();

        this.mTextColumnBuilder = new ArrayList();
        this.myCharts = new ArrayList();
    }

    /**
     * Constructor for create a report with custom title, header.
     * The footer will be the number of the current page.
     *
     * @param title  a String for the title of the report.
     * @param header a String for the header of the report.
     */
    public Report(String title, String header) {

        StyleBuilder boldStyle = stl.style().bold();
        StyleBuilder columnTitleStyle = stl.style().bold()
                .setBorder(stl.pen1Point())
                .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER)
                .setBackgroundColor(Color.LIGHT_GRAY);

        StyleBuilder valueStyle = stl.style().setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);

        this.mReport = report();
        this.mReport.title(cmp.text(title).setStyle(boldStyle))
                .pageHeader(cmp.text(header))
                .pageFooter(cmp.pageXofY().setStyle(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER))
                .setColumnTitleStyle(columnTitleStyle)
                .setColumnStyle(valueStyle)
                .highlightDetailEvenRows();

        this.mTextColumnBuilder = new ArrayList();
        this.myCharts = new ArrayList();
    }

    /**
     * Getter for fileName: the name for the report.
     *
     * @return a String with the filename.
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * This function is used to create and set the filename of the report.
     *
     * @param type a String used as memo for the used index.
     */
    public void setFileName(String type,MessageIndexCompute msg) {
        String currentDay = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss").format(LocalDateTime.now());
        if(msg.getUtente().equals(GlobalVariable.TopManager)){
            this.fileName = "src/main/resources/reportsTM/report_" + type + "_" + currentDay;
        }
        else if(msg.getUtente().equals(GlobalVariable.ManagerCentrale)){
            this.fileName = "src/main/resources/reportsMC/report_" + type + "_" + currentDay;
        }
        else {
            this.fileName = "src/main/resources/reportsML/report_" + type + "_" + currentDay;
        }
    }

    /**
     * Setter for the column of the report.
     *
     * @param mTextColumnBuilder an ArrayList of TextColumnBuilder user for the report.
     */
    public void setTextColumnBuilder(ArrayList<TextColumnBuilder> mTextColumnBuilder) {
        this.mTextColumnBuilder = mTextColumnBuilder;
    }

    /**
     * Setter for the data source.
     *
     * @param mDataSource a JRDataSource with the values for filling the report.
     */
    public void setDataSource(JRDataSource mDataSource) {
        this.mDataSource = mDataSource;
    }

    /**
     * This function is the core of the class and create the report.
     *
     * @param value an int that specify a set of charts: 1 LineChart
     *              2 BarChart3D
     *              4 AreaChart
     *              8 Difference (need to be added)
     *              16 TimeSeries (need to be added)
     *              For a combination of them use the sum.
     * @param tag   a String used for name the file.
     */
    public void createReport(int value, String tag,MessageIndexCompute msg) {

        setFileName(tag,msg);

        this.addColumnsToReport();

        switch (value) {
            case 1:
                addLineChart();
                break;

            case 2:
                addBarChart3D();
                break;

            case 3:
                addLineChart();
                addBarChart3D();
                break;

            case 4:
                addAreaChart();
                break;

            case 5:
                addAreaChart();
                addLineChart();
                break;

            case 6:
                addBarChart3D();
                addAreaChart();
                break;

            case 7:
                addLineChart();
                addBarChart3D();
                addAreaChart();
                break;


            case 8:
                addDifferenceChart();
                break;

            case 9:
                addLineChart();
                addDifferenceChart();
                break;

            case 10:
                addDifferenceChart();
                addBarChart3D();
                break;

            case 11:
                addLineChart();
                addBarChart3D();
                addDifferenceChart();
                break;

            case 12:
                addTimeSeriesChart();
                addDifferenceChart();
                break;

            case 13:
                addLineChart();
                addBarChart3D();
                addDifferenceChart();
                break;

            case 15:
                addLineChart();
                addBarChart3D();
                addTimeSeriesChart();
                addDifferenceChart();
                break;

            default:
                return;
        }

        try {
            // Only on one product add max, min, mean, std dev
            if(this.numberProd == 1){
                this.mReport.addSubtotalAtSummary(sbt.text("Max",this.mTextColumnBuilder.get(1))
                        , sbt.max(this.mTextColumnBuilder.get(2))).setSubtotalStyle(subtotalStyle())
                        .addSubtotalAtSummary(sbt.text("Min",this.mTextColumnBuilder.get(1))
                                , sbt.min(this.mTextColumnBuilder.get(2))).setSubtotalStyle(subtotalStyle())
                        .addSubtotalAtSummary(sbt.text("Media",this.mTextColumnBuilder.get(1))
                                , sbt.avg(this.mTextColumnBuilder.get(2))).setSubtotalStyle(subtotalStyle())
                        .addSubtotalAtSummary(sbt.text("Std Dev",this.mTextColumnBuilder.get(1))
                                , sbt.stdDev(this.mTextColumnBuilder.get(2))).setSubtotalStyle(subtotalStyle());
            }

            this.mReport.addSummary()
                    .addSummary(this.myCharts.toArray(new ComponentBuilder[this.myCharts.size()]))
                    .setSummarySplitType(SplitType.IMMEDIATE)
                    .groupBy(this.mTextColumnBuilder.get(0))
                    .setDataSource(this.mDataSource)
                    .toPdf(Exporters.pdfExporter(new File(this.fileName + ".pdf")))
                    .toHtml(Exporters.htmlExporter(new File(this.fileName + ".html")));
        } catch (DRException e) {
            e.printStackTrace();
        }

    }

    /**
     * This function is used for setting a style to subtotals rows.
     *
     * @return a StyleBuilder with the style.
     */
    private StyleBuilder subtotalStyle(){

        StyleBuilder myStyle = stl.style().bold()
                .setBackgroundColor(Color.CYAN)
                .setBorder(stl.pen(1.f, LineStyle.SOLID))
                .setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)
                .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        return myStyle;
    }

    /**
     * This function is used for add the Line Chart to the report.
     */
    private void addLineChart() {

        for (int i = 2; i < this.mTextColumnBuilder.size(); i++) {

            LineChartBuilder mLineChart = cht.lineChart();
            mLineChart.setCategory(this.mTextColumnBuilder.get(1));
            mLineChart.setTitle(this.mTextColumnBuilder.get(i).getName());
            mLineChart.customizers(new ChartCustomizer());

            mLineChart.series(cht.serie(this.mTextColumnBuilder.get(i)).setSeries(this.mTextColumnBuilder.get(0)));

            this.myCharts.add(cmp.verticalGap(50));
            this.myCharts.add(mLineChart);
            this.myCharts.add(cmp.verticalGap(50));
        }
    }

    /**
     * This function is used for add the Bar3D Chart to the report.
     */
    private void addBarChart3D() {

        for (int i = 2; i < this.mTextColumnBuilder.size(); i++) {

            Bar3DChartBuilder mBar3DChart = cht.bar3DChart();
            mBar3DChart.setCategory(this.mTextColumnBuilder.get(1));
            mBar3DChart.setTitle(this.mTextColumnBuilder.get(i).getName());
            mBar3DChart.customizers(new ChartCustomizer());

            mBar3DChart.series(cht.serie(this.mTextColumnBuilder.get(i)).setSeries(this.mTextColumnBuilder.get(0)));

            this.myCharts.add(cmp.verticalGap(50));
            this.myCharts.add(mBar3DChart);
            this.myCharts.add(cmp.verticalGap(50));
        }
    }

    /**
     * This function is used for add the TimeSeries Chart to the report.
     */
    private void addTimeSeriesChart() {

        for (int i = 2; i < this.mTextColumnBuilder.size(); i++) {

            TimeSeriesChartBuilder mTimeSeriesChart = cht.timeSeriesChart();
            mTimeSeriesChart.setTimePeriod(this.mTextColumnBuilder.get(1));
            mTimeSeriesChart.setTimePeriodType(TimePeriod.MONTH);
            mTimeSeriesChart.setTitle(this.mTextColumnBuilder.get(i).getName());
            mTimeSeriesChart.customizers(new ChartCustomizer());

            mTimeSeriesChart.series(cht.serie(this.mTextColumnBuilder.get(i)).setSeries(this.mTextColumnBuilder.get(0)));

            this.myCharts.add(cmp.verticalGap(50));
            this.myCharts.add(mTimeSeriesChart);
            this.myCharts.add(cmp.verticalGap(50));
        }
    }

    private void addAreaChart() {

        for (int i = 2; i < this.mTextColumnBuilder.size(); i++) {

            AreaChartBuilder mAreaChart = cht.areaChart();
            mAreaChart.setCategory(this.mTextColumnBuilder.get(1));
            mAreaChart.setTitle(this.mTextColumnBuilder.get(i).getName());
            mAreaChart.customizers(new ChartCustomizer());

            mAreaChart.series(cht.serie(this.mTextColumnBuilder.get(i)).setSeries(this.mTextColumnBuilder.get(0)));

            this.myCharts.add(cmp.verticalGap(50));
            this.myCharts.add(mAreaChart);
            this.myCharts.add(cmp.verticalGap(50));
        }
    }

    /**
     * This function is used for add the Difference Chart to the report.
     */
    private void addDifferenceChart() {

        for (int i = 2; i < this.mTextColumnBuilder.size(); i++) {

            DifferenceChartBuilder mDifferentChart = cht.differenceChart();
            mDifferentChart.setTimePeriod(this.mTextColumnBuilder.get(1));
            mDifferentChart.setTimePeriodType(TimePeriod.MONTH);
            mDifferentChart.setTitle(this.mTextColumnBuilder.get(i).getName());
            mDifferentChart.customizers(new ChartCustomizer());

            mDifferentChart.series(cht.serie(this.mTextColumnBuilder.get(i)).setSeries(this.mTextColumnBuilder.get(0)));

            this.myCharts.add(cmp.verticalGap(50));
            this.myCharts.add(mDifferentChart);
            this.myCharts.add(cmp.verticalGap(50));
        }
    }

    /**
     * This function is used for create a Date object used in Date columns.
     *
     * @param year  an int for the year.
     * @param month an int for the month.
     * @return a Date object with year and month
     */
    private Date toDate(int year, int month) {

        Calendar c = Calendar.getInstance();
        c.clear();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        return c.getTime();

    }

    /**
     * This function is used for add the Columns ti the report.
     */
    private void addColumnsToReport() {
        for (int i = 0; i < this.mTextColumnBuilder.size(); i++)
            this.mReport.addColumn(this.mTextColumnBuilder.get(i));
    }


    /**
     * This function is used for set the TextColumnBuilder for the table header.
     */
    public void setColumnForReport() {

        TextColumnBuilder<String> productColumn = col.column("Prodotto", "Prodotto", type.stringType());
        TextColumnBuilder<String> orderDateColumn = col.column("Data", "Data", type.stringType())
                .setStyle(stl.style().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER))
                .setStretchWithOverflow(Boolean.FALSE);

        TextColumnBuilder<Double> quantityColumn = col.column("Indice", "Indice", type.doubleType());

        this.mTextColumnBuilder.add(productColumn);
        this.mTextColumnBuilder.add(orderDateColumn);
        this.mTextColumnBuilder.add(quantityColumn);

    }

    /**
     * This function is used for insert the result calculated into the report.
     *
     * @param mResult              an ArrayList with the results.
     * @param mMessageIndexCompute a MessageIndexCompute with the informations about the index
     */
    public void setResultForReport(ArrayList<Result> mResult, MessageIndexCompute mMessageIndexCompute) {

        ArrayList<String> fieldName = new ArrayList();
        String period;

        for (int i = 0; i < this.mTextColumnBuilder.size(); i++)
            fieldName.add(this.mTextColumnBuilder.get(i).getName());

        switch (mMessageIndexCompute.getGranularity()) {
            case 1:
                period = "Mese";
                break;
            case 2:
                period = "Bimestre";
                break;
            case 3:
                period = "Trimestre";
                break;
            case 6:
                period = "Semestre";
                break;
            case 12:
                period = "Anno";
                break;
            default:
                period = "Error";
                break;
        }

        DRDataSource dataSource = new DRDataSource(fieldName.toArray(new String[fieldName.size()]));

        int j = 0;

        // nessun prodotto
        if(mMessageIndexCompute.getDescProduct().size() == 0) {
            for (Result prod : mResult) {
                int i = 1;
                for (Double var : prod.getVar()) {
                    dataSource.add("Grafico totale", Integer.toString(i) + "° " + period, var);
                    i++;
                }
                j++;
            }        }
        // hai prodotti
        else {
            for (Result prod : mResult) {
                int i = 1;
                for (Double var : prod.getVar()) {
                    dataSource.add(mMessageIndexCompute.getDescProduct().get(j), Integer.toString(i) + "° " + period, var);
                    i++;
                }
                j++;
            }
        }

        this.numberProd = mResult.size();
        this.setDataSource(dataSource);
    }


    private class ChartCustomizer implements DRIChartCustomizer, Serializable {
        private static final long serialVersionUID = 1L;

        @Override
        public void customize(JFreeChart chart, ReportParameters reportParameters) {
            CategoryAxis domainAxis = chart.getCategoryPlot().getDomainAxis();
            domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0));
        }
    }


    ///////////////////////////////////////////////////////////////////////////////
    //
    // Section for filling the report with static data
    //
    ///////////////////////////////////////////////////////////////////////////////

    public JRDataSource createTimeDataSource() {
        DRDataSource dataSource = new DRDataSource("product", "orderdate", "quantity", "price");

        dataSource.add("Product1", toDate(2010, 1), 50, new BigDecimal(200));
        dataSource.add("Product1", toDate(2010, 2), 110, new BigDecimal(450));
        dataSource.add("Product1", toDate(2010, 3), 70, new BigDecimal(280));
        dataSource.add("Product1", toDate(2010, 4), 250, new BigDecimal(620));
        dataSource.add("Product1", toDate(2010, 5), 100, new BigDecimal(400));
        dataSource.add("Product1", toDate(2010, 6), 80, new BigDecimal(320));
        dataSource.add("Product1", toDate(2010, 7), 180, new BigDecimal(490));

        dataSource.add("Product2", toDate(2010, 1), 20, new BigDecimal(250));
        dataSource.add("Product2", toDate(2010, 2), 210, new BigDecimal(400));
        dataSource.add("Product2", toDate(2010, 3), 40, new BigDecimal(320));
        dataSource.add("Product2", toDate(2010, 4), 200, new BigDecimal(600));
        dataSource.add("Product2", toDate(2010, 5), 180, new BigDecimal(200));
        dataSource.add("Product2", toDate(2010, 6), 90, new BigDecimal(380));
        dataSource.add("Product2", toDate(2010, 7), 130, new BigDecimal(490));

        dataSource.add("Product3", toDate(2010, 1), 25, new BigDecimal(50));
        dataSource.add("Product3", toDate(2010, 2), 10, new BigDecimal(40));
        dataSource.add("Product3", toDate(2010, 3), 100, new BigDecimal(30));
        dataSource.add("Product3", toDate(2010, 4), 140, new BigDecimal(60));
        dataSource.add("Product3", toDate(2010, 5), 250, new BigDecimal(75));
        dataSource.add("Product3", toDate(2010, 6), 130, new BigDecimal(88));
        dataSource.add("Product3", toDate(2010, 7), 185, new BigDecimal(127));
        return dataSource;
    }

    public void setColumnTimeSample() {
        ArrayList<TextColumnBuilder> myCol = new ArrayList();
        TextColumnBuilder<String> productColumn = col.column("Product", "product", type.stringType());
        TextColumnBuilder<Date> orderDateColumn = col.column("Order date", "orderdate", type.dateYearToMonthType());
        TextColumnBuilder<Integer> quantityColumn = col.column("Numero pezzi", "quantity", type.integerType());
        TextColumnBuilder<BigDecimal> priceColumn = col.column("Prezzo", "price", type.bigDecimalType());

        myCol.add(productColumn);
        myCol.add(orderDateColumn);
        myCol.add(quantityColumn);
        myCol.add(priceColumn);

        this.mTextColumnBuilder = myCol;
    }
}