package com.isssr_backend;

import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.thread.UpdateTask;
import com.isssr_backend.variables.util.ClassFinder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@ComponentScan
@EnableAutoConfiguration
@PropertySource("application.properties")
@SpringBootApplication
public class Start {

    public static void main(String[] args) {

        // necessaria allo start up
        ClassFinder.init();

        //necessaria alla temporizzazione
        UpdateTask task = new UpdateTask(GlobalVariable.dayInSec);

        // spring booter
        SpringApplication.run(Start.class, args);
    }
}