package com.isssr_backend.service.serviceFood;

import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.repository.RepositoryFood;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by GM on 04/07/2017.
 */
@Service
public class ServiceFoods {

    @Autowired
    private RepositoryFood mRepo_foods;

    public ArrayList<Foods> findAll() {
        return mRepo_foods.findAll();
    }

    public Foods addFoods(Foods food) {
        Foods f = mRepo_foods.save(food);
        return f;

    }

    public ArrayList<Foods> findAllByLocation(String location) {
        return mRepo_foods.findFoodsByLocation(location);
    }

    public Foods findByNameAndLocation(String foods, String location) {
        return mRepo_foods.findFoodsByNomeAndLocation(foods, location);
    }
}
