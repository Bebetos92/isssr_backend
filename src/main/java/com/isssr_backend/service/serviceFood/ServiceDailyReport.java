package com.isssr_backend.service.serviceFood;

import com.isssr_backend.entity.food.ReportDailyFoods;
import com.isssr_backend.repository.RepositoryDailyReportFoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by GM on 04/07/2017.
 */
@Service
public class ServiceDailyReport {

    @Autowired
    private RepositoryDailyReportFoods repositoryDailyReportFoods;

    public ArrayList<ReportDailyFoods> getAll() {
        return repositoryDailyReportFoods.findAll();
    }
}
