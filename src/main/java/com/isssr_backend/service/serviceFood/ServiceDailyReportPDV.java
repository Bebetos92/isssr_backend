package com.isssr_backend.service.serviceFood;

import com.isssr_backend.entity.food.DailyReportPDV;
import com.isssr_backend.repository.RepositoryDailyReportPDV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GM on 04/07/2017.
 */
@Service
public class ServiceDailyReportPDV {

    @Autowired
    private RepositoryDailyReportPDV repositoryDailyReportPDV;


    public DailyReportPDV addReport(DailyReportPDV d) {
        return repositoryDailyReportPDV.save(d);
    }

    public ArrayList<DailyReportPDV> findReport(Date d) {
        return repositoryDailyReportPDV.findDailyReportPDVByDataIs(d);
    }
}
