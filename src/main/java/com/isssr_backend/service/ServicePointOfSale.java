package com.isssr_backend.service;

import com.isssr_backend.entity.PointOfSale;
import com.isssr_backend.repository.RepositoryPointOfSale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by andre on 15/06/2017.
 */
@Service
public class ServicePointOfSale {


    @Autowired
    private RepositoryPointOfSale mRepo_PointOfSale;


    /**
     * This function returns all the PointOfSale in the database
     *
     * @return List<PointOfSale>
     */
    public List<PointOfSale> getAll() {
        return mRepo_PointOfSale.findAll();
    }

    /**
     * This function adds a new PointOfSale from the RequestBody
     *
     * @return PointOfSale ,the PointofSale just added
     */
    public PointOfSale addNewPOS(PointOfSale pointOfSale) {

        mRepo_PointOfSale.save(pointOfSale);
        return pointOfSale;
    }


}
