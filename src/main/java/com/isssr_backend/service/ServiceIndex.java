package com.isssr_backend.service;

import com.isssr_backend.entity.Index;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.parser.CYK.ParserCYK;
import com.isssr_backend.parser.IndexComputer;
import com.isssr_backend.repository.RepositoryIndex;
import com.isssr_backend.variables.util.ClassFinder;
import com.isssr_backend.variables.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;


@Service
public class ServiceIndex {

    @Autowired
    private RepositoryIndex mRepo_Index;

    @Autowired
    private AutowireCapableBeanFactory beanFactory;

    /**
     * This function is used for concatenating the GlobalVariables with the specific semantic variables.
     *
     * @param value a String used for getting the specific semantic.
     * @return
     */
    public static ArrayList<String> getVariableList(String value) {
        return ClassFinder.getVariables(value);
    }

    /**
     * GET call that return a List of all Indexes
     *
     * @return List<Index>
     */
    public ArrayList<Index> getIndexBySemantic(String category) {

        return mRepo_Index.findIndexByCategory(category);
    }

    /**
     * GET call that return a List of all Indexes
     *
     * @return List<Index>
     */
    public ArrayList<Index> getAll() {

        return mRepo_Index.findAll();
    }

    /**
     * POST call to add a new index giving the parameters in body
     *
     * @param index Index object to show if added in monitor
     * @return a String message as success/fail.
     */
    public String addNewIndex(Index index,HttpServletResponse response) {

        ParserCYK mParserCYK = new ParserCYK();

        if (mParserCYK.parse(index.getCategory(), index.getCalculate())) {
            System.err.println("Accepted new index : (" + index.getCategory() + ") ---> " + index.getCalculate());
            mRepo_Index.save(index);
            return "Index saved";
        }

        System.err.println("Refused index : (" + index.getCategory() + ") ---> " + index.getCalculate());
        response.setStatus(400);
        return "Error in index syntax.";
    }

    /**
     * This Controller is used for compute the Index.
     *
     * @param mIndexProd the MessageIndexCompute with all the information needed.
     * @return an ArrayList with all the results.
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public ArrayList<Result> computeIndex(MessageIndexCompute mIndexProd) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        // get correct index string
        String index = mRepo_Index.findByNameIndex(mIndexProd.getIndex()).getCalculate();

        // compute the real value of the index
        ArrayList<Result> result = new IndexComputer(beanFactory).computeIndex(index, mIndexProd);

        return result;
    }


}
