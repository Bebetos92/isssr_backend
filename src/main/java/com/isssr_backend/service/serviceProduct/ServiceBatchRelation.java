package com.isssr_backend.service.serviceProduct;

import com.isssr_backend.entity.product.BatchesRelation;
import com.isssr_backend.repository.RepositoryBatchRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by andre on 24/06/2017.
 */
@Service
public class ServiceBatchRelation {

    @Autowired
    RepositoryBatchRelation batchRelation;

    /**
     * Funzione che restituisce tutti i BatchRelation in un'arrayList
     *
     * @return ArrayList<BatchRelation>
     */
    public ArrayList<BatchesRelation> getAll() {

        return batchRelation.findAll();
    }

    public BatchesRelation addBatchRel(BatchesRelation b) {
        return batchRelation.save(b);
    }
}
