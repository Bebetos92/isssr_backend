package com.isssr_backend.service.serviceProduct;

import com.isssr_backend.entity.product.Batch;
import com.isssr_backend.repository.RepositoryBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by andre on 15/06/2017.
 */
@Service
public class ServiceBatch {

    @Autowired
    private RepositoryBatch mRepo_Batch;

    /**
     * Funzione che restituisce tutti i Batch
     *
     * @return List<Batch>
     */
    public List<Batch> findAll() {
        return mRepo_Batch.findAll();
    }


    /**
     * Funzione che salva un Batch
     *
     * @return void
     */
    public Batch addNewBatch(Batch batch) {
        return mRepo_Batch.save(batch);
    }

}
