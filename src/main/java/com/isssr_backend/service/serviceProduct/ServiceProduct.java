package com.isssr_backend.service.serviceProduct;

import com.isssr_backend.entity.ModelMapper.AbstractProducts;
import com.isssr_backend.entity.ModelMapper.Food;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.repository.RepositoryProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by andre on 15/06/2017.
 */
@Service
public class ServiceProduct {

    @Autowired
    private RepositoryProduct mRepo_Product;

    public Product findByDesc(String desc) {
        return mRepo_Product.findProductByDescriptionEquals(desc);
    }

    /**
     * Chiamata che restituisce tutti i prodotti  Metodo
     * sintassi GET
     * localhost:8080/product/findAll
     *
     * @return List<Product>
     */
    public List<Product> findAll() {

        AbstractProducts p = new Food("pizza", "secondo");
        p.toResume();

        return mRepo_Product.findAll();
    }


    /**
     * @param product Json impacchettato in questo modo :
     *                {
     *                "index": 0,
     *                "name": "vino",
     *                "price": 2.02,
     *                "category": [
     *                "duis",
     *                "non",
     *                "aliquip"
     *                ],
     *                "brand": "IDETICA",
     *                "expiredTime": "2014-01-23"
     *                }
     * @return Product aggiunto in JSON
     */

    public Product addNewProd(Product product) {

        mRepo_Product.save(product);
        return product;
    }

    /**
     * Funzione per crea nuova categoria che trova un insieme di prodotti contenenti caratteri passati nella get.
     *
     * @param fourchar Prime 3 lettere della ricerca
     * @return List<Product> Che appartengono alla categoria selezionata e che iniziano per le prime 3 lettere
     */

    public List<Product> riempiSelect(String fourchar) {

        return mRepo_Product.findProductByDescriptionContainingOrDescriptionContaining(fourchar.toLowerCase(), fourchar.toUpperCase());
    }

}
