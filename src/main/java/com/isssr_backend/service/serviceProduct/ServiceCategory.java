package com.isssr_backend.service.serviceProduct;

import com.isssr_backend.entity.food.CategoryFood;
import com.isssr_backend.entity.message.MessageNewCategory;
import com.isssr_backend.entity.product.Category;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.repository.RepositoryCategory;
import com.isssr_backend.repository.RepositoryCategoryFood;
import com.isssr_backend.repository.RepositoryFood;
import com.isssr_backend.repository.RepositoryProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by andre on 15/06/2017.
 */
@Service
public class ServiceCategory {

    @Autowired
    private RepositoryCategory mRepo_Category;
    @Autowired
    private RepositoryProduct mRepo_Product;

    @Autowired
    private RepositoryCategoryFood mRepo_CategoryFood;

    @Autowired
    private RepositoryFood mRepo_Food;


    /**
     * Funzione GET che restituisce tutte le categorie, usata per TEST
     *
     * @return List<Category>
     */
    public List<Category> getAll() {

        return mRepo_Category.findAll();

    }

    /**
     * Chiamata POST per aggiungere una nuova categoria usata per TEST
     *
     * @param category parametro passato dal frontend nel body
     * @ Category ritorna l'oggetto in JSON a schermo
     */
    public Category addNewCategory(Category category) {
        mRepo_Category.save(category);
        return category;
    }

    /**
     * Funzione usata per riempire la select categorie. Permette di identificare una categoria che comincia con una stringa str digitata
     *
     * @param str Parametro fornito per poter ricercare la categoria
     * @return
     */
    public List<Category> riempiSelectCategorie(String str, String utente) {
        return mRepo_Category.findCategoryByNameContainingAndUtenteOrUtenteAndNameContaining(str, utente, GlobalVariable.standard, str);
    }

    /**
     * Funzione che dati come parametro una categoria e una stringa di lettere restituisce il set di prodotti che contengono le 3 lettere
     * e che fanno parte della categoria
     *
     * @param category  parametro categoria precedentemente selezionato
     * @param threechar parametro delle prime 3 lettere
     * @return ArrayList<Product>
     */
    public ArrayList<Product> riempiSelectProdotti(String category, String threechar) {

        Category x = mRepo_Category.findCategoryByName(category);

        List<Product> prods = x.getProducts();
        ArrayList<Product> ret = new ArrayList<>();

        for (int i = 0; i < prods.size(); i++) {

            if (prods.get(i).getDescription().toLowerCase().contains(threechar.toLowerCase())) {
                ret.add(prods.get(i));
            }
        }
        return ret;
    }


    /**
     * Funzione che aggiunge categorie e prodotti in una nuova categoria ancora non definita (TEST)
     *
     * @param x MessageCategory
     * @return Categoria ritorna la nuova categoria
     */
    public Category addCustomCategory(MessageNewCategory x) {
        Category ret = new Category();
        CategoryFood retFood = new CategoryFood();
        ret.setName(x.getName());
        ret.setDescription(x.getDescription());
        ret.setUtente(x.getUtente());

        ArrayList<String> cat = new ArrayList<>();
        if (x.getCategorie() != null) {
            cat = x.getCategorie();
        }

        ArrayList<Product> prods = new ArrayList<>();
        if (x.getProdotti() != null) {
            for (int i = 0; i < x.getProdotti().size(); i++) {
                Product p = mRepo_Product.findProductByDescriptionEquals(x.getProdotti().get(i));
                prods.add(p);
            }
        }
        for (int i = 0; i < cat.size(); i++) {
            Category c = mRepo_Category.findCategoryByName(x.getCategorie().get(i));
            prods.addAll(c.getProducts());
        }

        //Codice per rimuovere i doppioni
        Set<Product> productDistict = new HashSet<>();
        productDistict.addAll(prods);
        prods.clear();
        prods.addAll(productDistict);
        ret.setProducts(prods);
        /*ArrayList<Category> cf = mRepo_Category.findCategoryByNameContainingAndUtenteOrUtenteAndNameContaining(x.getName(),x.getUtente(),
                GlobalVariable.standard,x.getName());
        if(cf!= null){
            for (Category tmpC : cf) {
                if (tmpC.getName().equals(x.getName())) {
                    ret.setId(tmpC.getId());
                    mRepo_Category.save(ret);
                }
            }
        }else{
            mRepo_Category.save(ret);
        }*/

        Category cf = mRepo_Category.findCategoriesByUtenteAndNameContaining(x.getUtente(), x.getName());
        if (cf != null) {
            ret.setId(cf.getId());
            mRepo_Category.save(ret);
        } else {
            mRepo_Category.save(ret);
        }
        return ret;
    }

    public void removeCategory(String name) {
        mRepo_Category.removeCategoryByName(name);
        return;
    }


}
