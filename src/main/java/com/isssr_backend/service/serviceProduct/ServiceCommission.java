package com.isssr_backend.service.serviceProduct;

import com.isssr_backend.entity.product.Commission;
import com.isssr_backend.repository.RepositoryCommission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by GM on 24/06/2017.
 */
@Service
public class ServiceCommission {
    @Autowired
    RepositoryCommission mRepo_commission;

    public ArrayList<Commission> findAll() {
        return mRepo_commission.findAll();
    }

    public Commission addNewCommission(Commission commission) {
        return mRepo_commission.save(commission);
    }

}
