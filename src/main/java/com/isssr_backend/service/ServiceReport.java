package com.isssr_backend.service;

import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.jasper.Report;
import com.isssr_backend.jasper.ResponseReport;
import com.isssr_backend.variables.util.Result;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;


@Service
public class ServiceReport {

    /**
     * This function is used for read all the content of a file.
     *
     * @param path     a String with the file path.
     * @param encoding a Charset for the encoding.
     * @return the file content as a String.
     * @throws IOException
     */
    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    /**
     * This function is used for get an ArrayList of all the reports saved.
     *
     * @return an ArrayList of String with all the file names.
     */
    public ArrayList<String> getAllReports(String utente) {

        File dir ;

        if(utente.equals(GlobalVariable.TopManager)){
            dir = new File("src/main/resources/reportsTM/");
        }
        else if(utente.equals(GlobalVariable.ManagerCentrale)){
            dir = new File("src/main/resources/reportsMC/");
        }
        else {
            dir = new File("src/main/resources/reportsML/");
        }


        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".pdf");
            }
        });

        ArrayList<String> fileName = new ArrayList();
        for (File x : files) {
            fileName.add(x.getName());
        }
        return fileName;
    }

    /**
     * This function is used for getting the lastest 5 reports created.
     *
     * @return an ArrayList with the file names.
     */
    public ArrayList<String> getLast5Report(String utente){

        File dir;
        if(utente.equals(GlobalVariable.TopManager)){
            dir = new File("src/main/resources/reportsTM/");
        }
        else if(utente.equals(GlobalVariable.ManagerCentrale)){
            dir = new File("src/main/resources/reportsMC/");
        }
        else {
            dir = new File("src/main/resources/reportsML/");
        }

        File[] pdfReports = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".pdf");
            }
        });

        Arrays.sort(pdfReports, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
            }
        });

        ArrayList<String> result = new ArrayList();

        int i=0;
        for(File x: pdfReports){
            if(i < 5){
                result.add(x.getName());
                i++;
            }
            if(i == 5)
                break;
        }

        return result;
    }

    /**
     * This function is used for delete a report .pdf file.
     *
     * @param name a String with the name of the file.
     * @return a message for the success/unsuccess of the operation.
     */
    public String deleteReport(String name,String utente) throws IOException {

        File filePdf;
        File fileHtml;
        File folder;

        if(utente.equals(GlobalVariable.TopManager)){
             filePdf = new File("src/main/resources/reportsTM/" + name + ".pdf");
             fileHtml = new File("src/main/resources/reportsTM/" + name + ".html");
             folder = new File("src/main/resources/reportsTM/" + name + ".html_files");
        }
        else if(utente.equals(GlobalVariable.ManagerCentrale)){
            filePdf = new File("src/main/resources/reportsMC/" + name + ".pdf");
            fileHtml = new File("src/main/resources/reportsMC/" + name + ".html");
            folder = new File("src/main/resources/reportsMC/" + name + ".html_files");
        }
        else {
            filePdf = new File("src/main/resources/reportsML/" + name + ".pdf");
            fileHtml = new File("src/main/resources/reportsML/" + name + ".html");
            folder = new File("src/main/resources/reportsML/" + name + ".html_files");
        }

        if (filePdf.delete() && fileHtml.delete()) {
            FileUtils.deleteDirectory(folder);
            return "File deleted";
        } else {
            return "File not deleted";
        }
    }

    /**
     * This function is used for download a report as .pdf file.
     *
     * @param fileName a String with the name of the report.
     * @param response a HttpServletResponse for sending the file.
     * @throws IOException
     */
    public void downloadFile(String utente, String fileName, HttpServletResponse response) throws IOException {
        File file = new File("src/main/resources/" + utente + "/" + fileName + ".pdf");

        if (!file.exists()) {
            String errorMessage = "Sorry. The file you are looking for does not exist";
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            return;
        }

        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
        response.setContentLength((int) file.length());
        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

    /**
     * This function is used for send an image.
     *
     * @param folder   a String with the folder.
     * @param filePath a String with the file name.
     * @param response a HttpServletResponse for sending the image.
     * @throws IOException
     */
    public void sendImg(String folder, String filePath, HttpServletResponse response,String utente) throws IOException {

        String fileName;

        if(utente.equals(GlobalVariable.TopManager)){

            fileName = "src/main/resources/reportsTM/" + folder + "/" + filePath + ".png";

        }
        else if(utente.equals(GlobalVariable.ManagerCentrale)){

            fileName = "src/main/resources/reportsMC/" + folder + "/" + filePath + ".png";

        }
        else {

            fileName = "src/main/resources/reportsML/" + folder + "/" + filePath + ".png";

        }

        File file = new File(fileName);

        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
        response.setContentLength((int) file.length());
        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

    /**
     * This function is used for create and send the report as .html file.
     *
     * @param mResult an ArrayList of Result calculated.
     * @param msg     the MessageIndexCompute with data requested from front-end.
     * @return a ResponseReport with filename and html content.
     * @throws IOException
     */
    public ResponseReport sendReport(ArrayList<Result> mResult, MessageIndexCompute msg) throws IOException {

        String title = "Report indice: " + msg.getIndex();
        String header = getHeaderWithDate(msg.getStart(), msg.getEnd());
        String footer = "";

        Report mNewReport = new Report(title, header, footer);
        mNewReport.setColumnForReport();
        mNewReport.setResultForReport(mResult, msg);
        mNewReport.createReport(7, msg.getIndex(),msg);

        String filePath = mNewReport.getFileName();
        ResponseReport mReport = new ResponseReport(filePath, readFile(filePath + ".html", Charset.forName("UTF-8")));

        return mReport;
    }

    /**
     * This function is used for create a String used as header for the report with start date and end date.
     *
     * @param start the Date when the analysis begins.
     * @param end   the Date when the analysis finishes.
     * @return the String used in the header with the params as String.
     */
    public String getHeaderWithDate(Date start, Date end) {
        String strStart = start.toString().substring(0, 11) + start.toString().substring(24);
        String strEnd = end.toString().substring(0, 11) + end.toString().substring(24);

        return "Periodo inizio: " + strStart + "\t\t\tPeriodo fine: " + strEnd;
    }

}
