package com.isssr_backend.thread;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;


/**
 * Created by andre on 27/06/2017.
 */
public class UpdateTask {

    private Timer timer1 = new Timer();
    private Timer timer2 = new Timer();
    private Timer timer3 = new Timer();
    private Timer timer4 = new Timer();


    /**
     * Questo Task imposta la data di esecuzione dell'update a mezzanotte
     *
     * @param seconds
     */
    public UpdateTask(int seconds) {
        //Get the Date corresponding to 11:01:00 pm today.
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();

        timer1.schedule(new UpdateMC(), time,seconds); //delay in milliseconds

        timer2.schedule(new UpdatePos(), time,seconds); //delay in milliseconds

        timer3.schedule(new UpdatePdv(), time,seconds); //delay in milliseconds

        timer4.schedule(new removeCacheTopThree(),time,1000*seconds);
    }

}
