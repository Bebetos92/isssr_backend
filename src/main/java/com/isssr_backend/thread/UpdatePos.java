package com.isssr_backend.thread;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.TimerTask;

/**
 * Created by andre on 29/06/2017.
 */
public class UpdatePos extends TimerTask {
    @Override
    public void run() {

        try {

            URL url = new URL("http://localhost:8080/util/updatePos");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
