package com.isssr_backend.parser;

import com.isssr_backend.variables.util.ClassFinder;

import java.util.ArrayList;

/**
 * Created by andre on 28/06/2017.
 */
public class ParserIndex {

    // valori delle priorità degli operatori speciali
    private static int sumSubPriority = 0;
    private static int mulDivPriority = 1;
    private static int exponentPriority = 2;
    private static int customFunctionPriority = -1;

    // valore del salto di priorità quando si entra/esce dalle parentesi
    private static int braketPriorityJump = 5;

    /**
     * Questa funzione prende un indice (sintatticamente corretto) e lo restituisce
     * sotto forma di sequenza di step matematici da calcolare.
     *
     * @param toParse stringa definente l'inddice
     * @return lista di step logici da eseguire
     */
    static public ArrayList<LogicElement> parse(String toParse) {

        // elimina eventuali spazi (non dovrebbero essercene)
        toParse = toParse.replaceAll(" ", "");

        // variabile da restituire
        ArrayList<LogicElement> ret = new ArrayList<>();

        // posizione di lettura stringa indice
        int i = 0;

        // priorità computazionale
        int priority = 0;

        // start parsing
        getStatement(toParse, i, priority, ret);

        return ret;
    }


    // funzione ricorsiva per parsare un blocco logico privo di parentesi
    private static int getStatement(String toParse, int i, int priority, ArrayList<LogicElement> ret) {

        boolean found;
        boolean justBackedIn;

        // per tutto l'indice
        while (i < toParse.length()) {

            justBackedIn = false;

            // stai entrando in una parentesi
            if (toParse.substring(i, i + 1).equals("(")) {
                i = getStatement(toParse, i + 1, priority + braketPriorityJump, ret);

                // a questo punto potresti aver finito
                if (i >= toParse.length()) {
                    break;
                }

                justBackedIn = true;
            }

            // stai uscendo da una parentesi
            if (toParse.substring(i, i + 1).equals(")")) {
                return i + 1;
            }

            // no ricerca variabili se sono appena uscito da una parentesi
            if(justBackedIn == false) {

                // number flag test reset
                found = false;

                for (String s : ClassFinder.getKnownVariables()) {

                    // testa se è una variabile nota
                    if (i + s.length() <= toParse.length() && found == false) {
                        if (toParse.substring(i, i + s.length()).equals(s)) {
                            i = getVariable(s, i, priority, toParse, ret);
                            found = true;
                        }
                    }
                }

                // se il controllo giunge quì, allora è un numero
                if (found == false) {

                    String number = "";

                    // leggi il numero sino ad incontrare un operatore matematico
                    while (i < toParse.length()) {

                        // se è una cifra leggila
                        if (validForNumber(toParse.substring(i, i + 1))) {
                            number += toParse.substring(i, i + 1);
                        }
                        // altrimenti esci
                        else {
                            break;
                        }

                        i++;
                    }
                    // aggiungi il numero alla lista dei LogicElement
                    ret.add(new LogicElement("constant", number, priority));
                }
            }

            // error detection
            if (i < 0) {
                System.exit(-1);
            }


            // se era la fine, esci
            if (i >= toParse.length()) {
                break;
            }

            // zona di lettura dell'operatore matematico

            // la parentesi non è un operatore
            if (!toParse.substring(i, i + 1).equals(")")) {

                // operatori + e -
                if (toParse.substring(i, i + 1).equals("+") || toParse.substring(i, i + 1).equals("-")) {
                    // carica l'operatore matematico
                    ret.add(new LogicElement("operator", toParse.substring(i, i + 1), priority));
                    i++;
                }

                // operatori * e / hanno priorità + 1
                else if (toParse.substring(i, i + 1).equals("*") || toParse.substring(i, i + 1).equals("/")) {
                    // carica l'operatore matematico
                    ret.add(new LogicElement("operator", toParse.substring(i, i + 1), priority + mulDivPriority));
                    i++;
                }


                // operatore ^ ha priorità + 2
                else if (toParse.substring(i, i + 1).equals("^")) {
                    // carica l'operatore matematico
                    ret.add(new LogicElement("operator", toParse.substring(i, i + 1), priority + exponentPriority));
                    i++;
                }

                // funzione non nota
                else {
                    System.err.println("ParserIndex.getStatement: unrecognised operator ["+ toParse.substring(i, i + 1) +"]. Abort.");
                    System.exit(-1);
                }
            }
            // esci ora dalla parentesi
            else {
                return i + 1;
            }
        }

        return i;
    }


    /**
     * Funzione utilizzabile per parsare product food e cat restituendo la varaibile i di incremento
     *
     * @param matcher
     * @param i
     * @param toParse
     * @param ret
     * @return i
     */
    static private int getVariable(String matcher, int i, int priority, String toParse, ArrayList<LogicElement> ret) {

        // salta il punto
        i += matcher.length() + 1;

        // ottieni la lista dei nomi delle classi compatibili con la semantica data
        ArrayList<String> variables = ClassFinder.getVariables(matcher);

        String var = new String("void");

        // ho trovato la classe che matcha la variabile
        for (String s : variables) {

            if (i + s.length() <= toParse.length()) {
                if (toParse.substring(i, i + s.length()).equals(s)) {
                    var = new String(s);
                    break;
                }
            }
        }

        // la classe implementante la variable non esiste
        if (var.equals("void")) {
            System.err.println("Errore nel parsing: impossibile trovare variabili per '" + matcher + "'");
            System.err.println("Parsing preview : '" + toParse.substring(i, i + 3) + "'");
            return -1;
        }
        // la classe implementante la variable esiste
        else {
            // istanzia la coppia
            LogicElement element = new LogicElement(matcher, var, priority);

            // aggiungila alla lista
            ret.add(element);

            // salta la lunghezza della var
            i += var.length();

            return i;
        }
    }


    // funzione che testa se il carattere può appartenere ad un numero
    private static boolean validForNumber(String character) {

        if (character.equals("0")) {
            return true;
        }
        if (character.equals("1")) {
            return true;
        }
        if (character.equals("2")) {
            return true;
        }
        if (character.equals("3")) {
            return true;
        }
        if (character.equals("4")) {
            return true;
        }
        if (character.equals("5")) {
            return true;
        }
        if (character.equals("6")) {
            return true;
        }
        if (character.equals("7")) {
            return true;
        }
        if (character.equals("8")) {
            return true;
        }
        if (character.equals("9")) {
            return true;
        }
        if (character.equals(".")) {
            return true;
        }

        return false;
    }


    public static int getSumSubPriority() {
        return sumSubPriority;
    }

    public static int getMulDivPriority() {
        return mulDivPriority;
    }

    public static int getExponentPriority() {
        return exponentPriority;
    }

    public static int getCustomFunctionPriority() {
        return customFunctionPriority;
    }
}
