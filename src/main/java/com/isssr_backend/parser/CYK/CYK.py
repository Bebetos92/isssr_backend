
import sys
import codecs


# dictionary of non-terminal symbols
nonterminals = []

basenonterminalsstart = 3000
basenonterminalsgap = 10000

nonterminalsMin = 0
nonterminalsMax = 0


allow_prints = 1

#custom print
def print_m(s, end = "\n"):
    if(allow_prints):
        print (s,end = end)



# generate a lot of non terminals
def populate_non_terminals():

	global nonterminals

	i = nonterminalsMin
	lim = nonterminalsMax

	while(i < lim):
		nonterminals.append(chr(i))
		i = i + 1


# check if a symbol is a non-terminal
def check_is_non_terminal(term):
	if(term in nonterminals):
		return 1
	else:
		return 0



# check that nonterminals are uppercase only
def check_nt(grammar):
    for a in range(len(grammar[1])):
        if(not check_is_non_terminal(grammar[1][a][0])):
            print_m("Error, one or more productions had terminal symbols on the left.")
            sys.exit()


# function used to be sure the grammar is acceptable
def check_syntax(file_data):

    error = 0

    # check the private symbols are not used
    if('$' in file_data):
        print_m("Error :'$' is a private symbol and cannot be used.")
        error = 1

    if('#' in file_data):
        print_m("Error :'#' is a private symbol and cannot be used.")
        error = 1

    if(error):
        return

# function used to check if a symbol produces in some steps the epsilon symbol
def star_epsilon(grammar,  symbol,  excluded_list = []):

    #print_m('in ' + symbol)

    for a in range(len(grammar)):
        if(grammar[a][0] == symbol):
            for b in range(len(grammar[a][1])):
                if(check_is_non_terminal(grammar[a][1][b])):
                    # dont loop on yourself
                    if(symbol != grammar[a][1][b] and not symbol in excluded_list):
                        exclme = excluded_list
                        exclme.append(symbol)
                        #print_m('recur ' + grammar[a][1][b])
                        #print_m('excl : '+ str(exclme))
                        found = star_epsilon(grammar,  grammar[a][1][b],  exclme)
                        exclme.pop()
                        if(found == 1):
                            #print_m('out 1')
                            return 1
                else:
                    if(grammar[a][1][b] == '&'):
                        #print_m('out 0')
                        return 1
    #print_m('out 0')
    return 0


# function used to clear all blank spaces
def replace_all(line, was,  will):
    while(was in line):
        line = line.replace(was,  will)
    return line

# read the input from the text file
def getdata():
    name = input("Insert the name of the input file : ");
    try:
        file = open(name, "r")
    except IOError:
        print_m("Error : file is missing")
        return "no_such_file"

    name = file.read()
    file.close()

    return name

# function used to parse and extract the grammar
def retrieve_grammar(text):


    # check file is compatible
    if( "__AXIOM__" in text):

        if(text[len(text)-1] != "\n"):
            text += "\n"

        data = []

        # get axiom
        ind = text.index("__AXIOM__") # take __AXIOM__ index
        ind += len("__AXIOM__") + 1 # slip __AXIOM__ and the '=' symbol
        axiom = text[ind] # only one character symbols allowed

        # save axiom
        data.append(axiom)

        # make new search index
        i = ind + 1

        # check parsing compatibility
        if(text[i] != "\n"):
            print_m("Error, file format unrecognised")
            sys.exit()

        # get all the productions
        prod_complete = []

        lim = len(text)

        while( i < lim):

	    # skip comment line
            if(text[i] == '%'):
                while(text[i] != '\n' and i < lim):
                    i += 1

            if(text[i] != '\n'):
                nonterminal = text[i] # get X nonterminal
                prod = ''
                i+= 1

                # check syntax
                if(i < lim - 2):
                    if(text[i:i+2] != '->'):
                        print_m("Error, expected production arrow, after non-terminal '" + text[i-1:i] + "' but else was found")
                        print_m("Remembrer that the arrow of the production must use the '->' symbols.")
                        sys.exit()
                else:
                    print_m("Error, missing right parameter of production.")
                    print_m("Remember that espilon productions use the special symbol '&'")
                    sys.exit()

                i+= 2 # skip X-> format

                # instaget epsilon production
                if(text[i] == '\n' or text[i] == '|'):
                    print_m("Error, missing right parameter of production.")
                    print_m("Remember that espilon production use the special symbol '&'")
                    sys.exit()
                else:
                    while(1):
                        while(text[i] != '\n' and text[i] != '|'):
                            prod += text[i]
                            i+=1

                        prod_complete.append([nonterminal,  prod])
                        prod = ''

                        if(text[i] == '\n' or not i < lim):
                            break
                        i+= 1

            else:
                i += 1

    else:
        print_m("Error, axiom flag is missing.")
        sys.exit()


    # save productions
    data.append(prod_complete)

    return data


# function used to obtain the grammar
def obtain_grammar(file_data):

    # check that private symbols are not used
    check_syntax(file_data)

    # clean from unnecessary blank spaces
    file_data = replace_all(file_data,  "\t",  "")
    file_data = replace_all(file_data,  " "  ,  "")

    # parse the grammar
    grammar = retrieve_grammar(file_data)

    # check the grammar is valid
    check_nt(grammar)

    return grammar

# function used to get a list of producers, given a list of producted symbols and the grammar
def get_producers(prod_list,  grammar):

    # backgive this list
    result = []

    # for all producted symbols
    for a in range(len(prod_list)):
        # search the whole grammar
        for b in range(len(grammar)):
            if(grammar[b][1] == prod_list[a]):
                result.append(grammar[b][0])
    return result


# function used to buld the first line
def build_first_line(word,  grammar):

    matrix = []

    for a in range(len(word)):
        producers = get_producers([word[a]], grammar)
        matrix.append(producers)

    return matrix

# function used to get the triangular producer list
def test_production(list_1,  list_2,  grammar):
    producers = []
    for a in range(len(grammar)):
        if(grammar[a][1][0] in list_1 and grammar[a][1][1] in list_2):
            producers.append(grammar[a][0])
    return producers

# function used to print_m the acceptance matrix
def print_m_acc_matrix(matrix,  size):

    for a in range(size):
        print_m('')
        for b in range(size):
            print_mme = str(matrix[a*size + b])
            print_m(print_mme,  end = " ")

    print_m("\n________________________________\n")

# function used to append elements of lists
def full_append(l1, l2):
    for a in range(len(l2)):
        if(not l2[a] in l1):
            l1.append(l2[a])



# function used to obtain the acceptance matrix
def matrix_builder(word,  grammar,  axiom):

    # check the epsilon word
    if(len(word) == 0):
        if([axiom, '&'] in grammar):
            return -1
        else:
            return -2

    # size of word
    size = len(word)

    # init the matrix first line
    matrix =  build_first_line(word,  grammar)
    for a in range(size*(size-1)):
        matrix.append([])


    # dual-loop builder
    for i in range(1, size):
        for j in range(size - i):

            allprod = []

            # internal-loop-tester
            for k in range(i):

                prod_list_1 = matrix[(k)*size +  j ]
                prod_list_2 = matrix[(i-k-1)*size +  j + k + 1]

                # test triangolar production
                producers = test_production(prod_list_1,  prod_list_2,  grammar)

                # full append with no doubles
                if(producers != []):
                    full_append(allprod, producers)

            # init the result
            matrix[i*size + j] = allprod

    # print_m the matrix
    # print_m_acc_matrix(matrix,  size)

    # tell the result ( if axiom in top position )
    if(axiom in matrix[size*(size-1)]):
        return 1
    else:
        return 0




# function used to obtain a free symbol
def get_free_symbol(taken_list):

    for a in range(len(nonterminals)):
        if(not nonterminals[a] in taken_list):
            return nonterminals[a]


    # add 100 more non terminals to the non-terminals set
    global nonterminalsMin
    global nonterminalsMax
    nonterminalsMin = nonterminalsMax
    nonterminalsMax = nonterminalsMax + 100

    populate_non_terminals()

    # first newly generated non terminal
    return nonterminals[nonterminalsMin - basenonterminalsstart]



# function used to retrieve all the productions of a given symbol
def retrieve_all_productions(grammar,  symbol):
        listis = []
        for a in range(len(grammar)):
            if(grammar[a][0] == symbol):
                listis.append(grammar[a][1])
        return listis



# function used to apply the step 1
def step_1(grammar, taken_symbol):

    # get rid of production in the form of  X-> Za | aZ etc
    for a in range(len(grammar[1])):

        for b in range(len(grammar[1][a][1])):
            if(not check_is_non_terminal(grammar[1][a][1][b]) and len(grammar[1][a][1]) != 1):

                # get free symbol
                newsymbol = get_free_symbol(taken_symbol)
                taken_symbol.append(newsymbol)

                # add new production
                addme = [ newsymbol,  grammar[1][a][1][b]]

                # replace old production
                list_version = list(grammar[1][a][1])
                list_version[b] = newsymbol
                grammar[1][a][1] = ''.join(list_version)

                # -------------------------------------
                # insert the newly obtained production in the old grammar
                grammar[1].append(addme)
                addme = []


# function used to apply the step 2
def step_2(grammar,  taken_symbol):

    # shorten until X->YZ any production in the for X -> YZK...NH
    donesomething = 1
    while(donesomething):
        donesomething = 0
        doit = len(grammar[1])
        for a in range(doit):

            if(len(grammar[1][a][1]) > 2 ):
                while(len(grammar[1][a][1]) > 2 ):
                    # get free symbol
                    newsymbol = get_free_symbol(taken_symbol)

                    if(len(newsymbol)>1):
                        print_m('Error, too many symbols required. This software uses only')
                        print_m("uppercase letters as nonterminals. ( max nonterminals = 26)")
                        sys.exit()
                    taken_symbol.append(newsymbol)

                    # add new production
                    addme = [ newsymbol,  grammar[1][a][1][0] + grammar[1][a][1][1] ]

                    # replace old production
                    list_version = list(grammar[1][a][1])
                    list_version = list_version[1:]
                    list_version[0] = newsymbol
                    grammar[1][a][1] = ''.join(list_version)

                    # -------------------------------------
                    donesomething = 1
                    # insert the newly obtained production in the old grammar
                    grammar[1].append(addme)
                    addme = []


# function used to applu the step 3
def step_3(grammar):

    it_is = 0

    # check if axiom is on right side
    for a in range(len(grammar[1])):

        for b in range(len(grammar[1][a][1])):
            if(grammar[0] == grammar[1][a][1][b]):
                it_is = 1

    if(it_is):
        # obtain the extended grammar
        grammar[1].append(['$', grammar[0]])
        grammar[0] = '$'



# function used to apply the step 4
def step_4(grammar):

    found_an_epsilon = 0

    for a in range(len(grammar[1])):

        # if the right side is 2 elements long
        if(len(grammar[1][a][1]) > 1):
            for b in range(2):

                # if the symbol is a nonterminal that produces epsilon
                if(star_epsilon(grammar[1], grammar[1][a][1][b])):
                    found_an_epsilon = 1
                    # if was first element
                    if(b == 0):
                        # add a new shortcut production
                        grammar[1].append([grammar[1][a][0], grammar[1][a][1][1]])
                    # else was the second
                    else:
                        # add a new shortcut production
                        grammar[1].append([grammar[1][a][0], grammar[1][a][1][0]])

    # get rid of all the epsilon productions from the productions set
    do = 1
    while(do):
        do = 0
        for a in range(len(grammar[1])):

            if(grammar[1][a][1] == '&' and grammar[1][a][0] != grammar[0]):
                del grammar[1][a]
                do = 1
                break

    # erease eventual loop-productions
    do = 1
    while(do):
        do = 0
        for a in range(len(grammar[1])):
            if(grammar[1][a][0] == grammar[1][a][1]):
                del grammar[1][a]
                do = 1
                break

    return found_an_epsilon



# function used to apply the step 5
def step_5(grammar):

    # if any production of the form X -> Y, then Y -> KK i'll obtain X -> KK
    future_delete = []

    do = 1
    while(do):
        do = 0
        for a in range(len(grammar[1])):

            # found a unit production
            if(len(grammar[1][a][1]) == 1 and  check_is_non_terminal(grammar[1][a][1])):
                productions = retrieve_all_productions(grammar[1], grammar[1][a][1])

                # build a new production for each one
                for b in range(len(productions)):
                    # no doubles
                    if( not [grammar[1][a][0], productions[b]] in grammar[1]):
                        grammar[1].append([grammar[1][a][0], productions[b]])
                        do = 1

                # add to kill list all the productions related to the unit production
                if( not grammar[1][a][1] in future_delete):
                    future_delete.append(grammar[1][a])

    # delete all the unit productions
    done_something = 1
    while(done_something):
        done_something = 0
        for a in range(len(grammar[1])):

            if(grammar[1][a] in future_delete):
                del grammar[1][a]
                done_something = 1
                break

# convert the given grammar into the equivalent chomsky normal form
def chomsky_normal_form(grammar):

    taken_symbol = []

    # get the list of already taken symbols
    for a in range(len(grammar[1])):
        if(not grammar[1][a][0] in taken_symbol):
            taken_symbol.append(grammar[1][a][0])

    # ----------------------------------------------------------------------------------------------------------------------------------
    # STEP 1 ( get rid of multy-terminal production ) A -> yY
    step_1(grammar, taken_symbol)


    # ----------------------------------------------------------------------------------------------------------------------------------
    # STEP 2 ( make all production of nonterminal of size 2 ) A -> XXXX ... X  to A -> YK
    step_2(grammar, taken_symbol)


    # ----------------------------------------------------------------------------------------------------------------------------------
    # STEP 3 ( extend the grammar ) make $ the axiom
    step_3(grammar)


    # ----------------------------------------------------------------------------------------------------------------------------------
    # STEP 4 ( get rid of epsilon productions )
    epsilon_was_found = step_4(grammar)

    if(epsilon_was_found):
        grammar[1].append([grammar[0], '&'])



    # ----------------------------------------------------------------------------------------------------------------------------------
    # STEP 5 ( get rid of the unit productions ) X -> Y ^ Y -> KK ----> X -> KK
    step_5(grammar)




# function used to better display a grammar data
def print_m_g(grammar):

    s = ""
    for a in range(len(grammar[1])):
        s += grammar[1][a][0]
        s += " -> "
        s += grammar[1][a][1] + "\n"

    f = open('test', 'w')
    f.write(s)
    f.close()


# result print_ming function
def tellresult(result):

    if(result == -2):
        print_m('The empty string does not belong to this grammar.')
    elif(result == -1):
        print_m('The empty string belongs to this grammar.')
    elif(result == 0):
        print_m('The given word does not belong to the given grammar.')
    elif(result == 1):
        print_m('The given word belongs to the given grammar.')

    print_m("")



# main call
def mainlogic():

    # get raw grammar
    file_data = getdata()

    if(file_data == "no_such_file"):
        return

    # get the grammar
    grammar = obtain_grammar(file_data)

    print_m("Here is the parsed grammar :")
    print_m_g(grammar)

    # convert the given grammar into chomsky normal form
    chomsky_normal_form(grammar)

    print_m('Here is the equivalent chomsky normal form : ')
    print_m_g(grammar)

    reparse = 'y'
    while(reparse == 'y'):

        # get the word
        word = input('Please, insert the word to test : ')

        # do the parsing
        result = matrix_builder(word,  grammar[1],  grammar[0])

	# output parsing result
        tellresult(result)

        reparse = input('Parse another word? [y, else] : ')




# main call 2
def mainlogic2(file_data):

    # get the grammar
    grammar = obtain_grammar(file_data)

    # convert the given grammar into chomsky normal form
    chomsky_normal_form(grammar)

    while(1):

        # get the word
        word = input('Please, insert the word to test : ')

        # do the parsing
        result = matrix_builder(word,  grammar[1],  grammar[0])

	# output parsing result
        tellresult(result)



# initialize the non terminal base
def initsymbols():
    global nonterminalsMin
    global nonterminalsMax

    # setup the global values
    nonterminalsMin = basenonterminalsstart
    nonterminalsMax = basenonterminalsstart + basenonterminalsgap

    # generate a base set of non terminal symbols
    populate_non_terminals()




# quick launcher
def run(fname):

    # get non terminals base
    initsymbols()

    try:
        file = open(fname, "r")
        fcontent = file.read()
        file.close()

        # with arg, it does not read file later
        mainlogic2(fcontent)

        return 0

    except IOError:
        input("Exception ({0}): {1}".format(e.errno, e.strerror))
        return 1




# loop on the main call
def mainloop():

    # get non terminals base
    initsymbols()

    try:

	# program main logic
        mainlogic()
        ask = input("Do you want to open a new file? [y, else] : ")

	# repeat main logic ?
        while(ask == "y"):
            mainlogic()
            ask = input("Do you want to open a new file? [y, else] : ")

        return 0

    except Exception as e:
        input("Exception ({0}): {1}".format(e.errno, e.strerror))
        return 1




# one word test
def parseone(word):

    global allow_prints
    allow_prints = 1

    # get non terminals base
    initsymbols()

    try:
        
        file = open("grammar.txt", "r", encoding='utf-8')
        fcontent = file.read()
        file.close()

	# get the grammar
        grammar = obtain_grammar(fcontent)

	# convert the given grammar into chomsky normal form
        chomsky_normal_form(grammar)

        # do the parsing
        result =  matrix_builder(word,  grammar[1],  grammar[0])

        return result

    except IOError as e:
        input("Exception ({0}): {1}".format(e.errno, e.strerror))
        return 1



if (__name__ == "__main__"):
    print(sys.argv[1])
    if(parseone(sys.argv[1])):
        print_m("1")
        sys.exit(1)
    else:
        print_m("0")
        sys.exit(0)






