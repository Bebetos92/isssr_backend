package com.isssr_backend.parser;

import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;

/**
 * Created by andre on 28/06/2017.
 */
public class LogicElement {

    private String oggetto;
    private String info;
    private int priority;
    private ArrayList<Result> partials;

    // costruttore per popolare il logic element con i riferimenti logici
    public LogicElement(String oggetto, String info, int priority) {
        this.oggetto = oggetto;
        this.info = info;
        this.priority = priority;
    }

    // costruttore per popolare il logic element con una lista di valori
    public LogicElement(String oggetto, ArrayList<Result> partials, int priority) {
        this.oggetto = oggetto;
        this.partials = partials;
        this.priority = priority;
    }

    public String getOggetto() {
        return oggetto;
    }

    public void setOggetto(String oggetto) {
        this.oggetto = oggetto;
    }

    public ArrayList<Result> getPartials() {
        return partials;
    }

    public void setPartials(ArrayList<Result> partials) {
        this.partials = partials;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
