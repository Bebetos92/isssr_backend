package com.isssr_backend.parser;

import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.variables.Variable;
import com.isssr_backend.variables.VariableFactory;
import com.isssr_backend.variables.util.Result;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import java.util.ArrayList;

/**
 * Created by emanuele on 03/07/17.
 */
public class IndexComputer {

    // variabile di supporto per forzare gli autowired
    private AutowireCapableBeanFactory beanFactory;

    // ctor
    public IndexComputer(AutowireCapableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    // popola elementi logici con dati veri
    private void populateLogicElements(ArrayList<LogicElement> elements, MessageIndexCompute mIndexProd) {

        // populate all products with the correct prod
        for (LogicElement e : elements) {

            // è un prodotto
            if (e.getOggetto().equals("product")) {

                // get new instance
                Variable var = VariableFactory.getVariable(e.getInfo());

                // forza l'ingetion delle variabili autowired
                beanFactory.autowireBean(var);

                // populate it as single
                if (mIndexProd.getFlag().equals("")) {
                    var.computeProduct(mIndexProd);
                    e.setPartials(var.getValue());
                }
                // populate it as category
                else {
                    var.computeCategoryProduct(mIndexProd);
                    e.setPartials(var.getValueCategory());
                }
            }


            // è un food
            if (e.getOggetto().equals("food")) {

                // get new instance
                Variable var = VariableFactory.getVariable(e.getInfo());

                // forza l'ingetion delle variabili autowired
                beanFactory.autowireBean(var);

                // populate it as single
                if (mIndexProd.getFlag().equals("")) {
                    var.computeFood(mIndexProd);
                    // allocate it
                    e.setPartials(var.getValue());
                }
                // populate it as category
                else {
                    var.computeCategoryFood(mIndexProd);
                    // allocate it
                    e.setPartials(var.getValueCategory());
                }
            }

            // è una global
            if (e.getOggetto().equals("economic")) {

                // get new instance
                Variable var = VariableFactory.getVariable(e.getInfo());

                // forza l'ingetion delle variabili autowired
                beanFactory.autowireBean(var);

                // populate it
                var.computeEconomic(mIndexProd);

                // allocate it
                e.setPartials(var.getValueEconomics());
            }
        }
    }


    // funzione che ottenuta una stringa indice la parsa e restituisce il valore dell'indice come
    // vettore di double.
    public ArrayList<Result> computeIndex(String index, MessageIndexCompute mIndexProd) {

        // via gli spazi
        index = index.replaceAll(" ", "");

        System.err.println("About to parse index : " + index);

        // parse index string into logic element vector
        ArrayList<LogicElement> elements = ParserIndex.parse(index);

        // debug print
        postLogicStructure(elements);

        // populate logic element list with real data
        populateLogicElements(elements, mIndexProd);

        // operate the logic element vector to obtain the final resultProduct vector
        ArrayList<Result> finalResult = LogicEngine.operateList(elements);

        return finalResult;
    }

    // debug call
    public static void postLogicStructure(ArrayList<LogicElement> elements) {
        System.err.println("Logic elements list");
        for(LogicElement e: elements){
            System.err.println(e.getInfo() + " : " + e.getOggetto() + "["+e.getPriority()+"]");
        }
    }
}
