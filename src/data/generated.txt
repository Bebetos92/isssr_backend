JSON SCHEMA PRODOTTO MAGAZZINOC.

[
  '{{repeat(100)}}',
  {
    index: '{{index()}}',
    name: function (tags) {
    var prodotti = ['apple', 'banana', 'strawberry','vino','birra','tonno','agnello',
                   'acqua','cocacola', 'pasta', 'pasta all uovo'];
      return prodotti[tags.integer(0, prodotti.length - 1)];
    },
    price: '{{floating(0, 20, 2, "0,0.00")}}',
	category: [
      '{{repeat(3)}}',
      '{{lorem(1, "words")}}'
    ],
    brand: '{{company().toUpperCase()}}',
    expiredTime: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-ddThh:mm:ss.000")}}'
  }
]

JSON PRODOTTO PDV LAVORATO
[
  '{{repeat(100)}}',
  {
    index: '{{index()}}',
    name: function (tags) {
    var prodotti = ['pasta al tonno', 'pasta al pomodoro' , 'tonno alla piastra' , 'tiramisu', 'patatine fritte', 'barbera', ' chardonnay' ,'cicoria'];
      return prodotti[tags.integer(0, prodotti.length - 1)];
    },
    category: function (tags) {
    var categorie = ['primi', 'secondi', 'drink','dolci', 'contorni'];
      return categorie[tags.integer(0, categorie.length - 1)];
    },
    tags:'{{lorem(1, "words")}}',
    price: '{{floating(1, 25, 2, "0,0.00")}}',    
    brand: '{{company().toUpperCase()}}',
    qMade: '{{integer(5, 25)}}',
    qSell: '{{integer(0,this.qMade)}}',
    DayOfSell: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-dd")}}'
  }
]

[
  '{{repeat(100)}}',
  {
    _id: '{{objectId()}}',
    indexProdotto: '{{index()}}',
    quantity: '{{integer(5,100)}}',
    price:  '{{floating(1.5 , 200, 2, "0,0.00")}}',

    expiredTime: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-ddThh:mm:ss.000")}}',
  }
]

Food Generatore
[
  '{{repeat(100)}}',
  {

    name: function (name) {
    var prodotti = ['apple', 'banana', 'strawberry','vino','birra','tonno','agnello',
                   'acqua','cocacola', 'pasta', 'pasta all uovo'];
      return prodotti[name.integer(0, prodotti.length - 1)];
    },
    mainCategory: function(mainCategory){
      var category = ['aperitivo','antipasto','primi','secondo','contorno','frutta','dolce', 'bevanda'];
      return category[mainCategory.integer(0,category.length-1)];
    },
    tags: [
      '{{repeat(3)}}',
      function(tags){
      var tag = ['pesce','vegan','spicy','salty','surgelato','fresco','tradizionale', 'alcolico'];
      return tag[tags.integer(0,tag.length-1)];
    }],

    recipe : '{{lorem(1, "words")}}',
    expiredTime: '{{integer(0, 3)}}'
  }
]




